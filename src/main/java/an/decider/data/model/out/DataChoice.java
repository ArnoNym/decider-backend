package an.decider.data.model.out;

import an.decider.data.model.IdModel;
import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Opinion;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
public class DataChoice implements IdModel {
    @Id
    private String id;

    @ManyToOne
    private Decision decision;

    @OneToMany(mappedBy = "dataChoice", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DataGroup> dataGroupList = new ArrayList<>();

    private boolean modified = false;

    @Transient
    private List<Demander> unusedDemanders = new ArrayList<>();

    public DataChoice() {
    }

    public DataChoice(@NotNull List<DataGroup> dataGroupList) {
        this.dataGroupList = dataGroupList;
    }

    public double calcUtility() {
        double utility = 0;
        for (DataGroup dataGroup : dataGroupList) {
            utility += dataGroup.calcUtility();
        }
        return utility;
    }

    public int calcMaxMemberCount() {
        int maxMemberCount = 0;
        for (DataGroup dataGroup : dataGroupList) {
            int memberCount = dataGroup.getDataMemberList().size();
            if (memberCount > maxMemberCount) {
                maxMemberCount = memberCount;
            }
        }
        return maxMemberCount;
    }

    public int calcDemanderCount() {
        int demanderCount = unusedDemanders.size();
        for (DataGroup dataGroup : dataGroupList) {
            demanderCount += dataGroup.getDataMemberListSize();
        }
        return demanderCount;
    }

    public int calcCreateRowsCount() {
        int demanderCount = calcDemanderCount();
        int rowsCont = 0;
        for (DataGroup dataGroup : dataGroupList) {
            rowsCont = Math.max(rowsCont, dataGroup.calcShowSpacesCount(demanderCount));
        }
        return rowsCont;
    }

    public List<Opinion> getAllOpinionList() {
        List<Opinion> allOpinionList = new LinkedList<>();
        for (DataGroup dataGroup : dataGroupList) {
            List<DataMember> dataMemberList = dataGroup.getDataMemberList();
            for (DataMember dataMember : dataMemberList) {
                allOpinionList.addAll(dataMember.getDemander().getOpinionList());
            }
        }
        for (Demander demander : unusedDemanders) {
            allOpinionList.addAll(demander.getOpinionList());
        }
        return allOpinionList;
    }

    public List<DataMember> calcMemberList() {
        List<DataMember> allMemberList = new ArrayList<>();
        for (DataGroup dataGroup : dataGroupList) {
            List<DataMember> memberList = dataGroup.getDataMemberList();
            if (memberList != null) {
                allMemberList.addAll(memberList);
            }
        }
        return allMemberList;
    }

    /*
    /**
     * @return All opinions on offers in this group.
     *
    public List<Opinion> getAllRelevantOpinionList() {
        Set<String> thisChoicesOfferIds = new HashSet<>();
        for (DataGroup dataGroup : dataGroupList) {
            thisChoicesOfferIds.add(dataGroup.getOffer().getId());
        }

        List<Opinion> allOpinionList = new LinkedList<>();
        for (DataGroup dataGroup : dataGroupList) {
            for (DataMember dataMember : dataGroup.getDataMemberList()) {
                for (Opinion opinion : dataMember.getDemander().getOpinionList()) {
                    if (thisChoicesOfferIds.contains(opinion.getOffer().getId())) {
                        allOpinionList.add(opinion);
                    }
                }
            }
        }
        return allOpinionList;
    }
    */

    // ================================================================================================================

    public void validate() {
        if (dataGroupList.isEmpty()) throw new IllegalStateException();
        for (DataGroup dataGroup : dataGroupList) {
            dataGroup.validate();
        }
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof DataChoice)) return false;
        IdModel idModel = (IdModel) obj;
        return id.equals(idModel.getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Utility: ").append(calcUtility());
        for (DataGroup dataGroup : dataGroupList) {
            sb.append("\n-> ").append(dataGroup.toString());
        }
        return sb.toString();
    }
}
