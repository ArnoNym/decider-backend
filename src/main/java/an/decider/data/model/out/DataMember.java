package an.decider.data.model.out;

import an.decider.calc.input.Offer;
import an.decider.calc.output.Member;
import an.decider.data.model.IdModel;
import an.decider.data.model.input.Demander;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Data
public class DataMember implements IdModel, Member {
    @Id
    private String id;

    @ManyToOne
    private Demander demander;

    @ManyToOne
    private DataGroup dataGroup;

    @Nullable
    @Transient
    private Double addDemanderBonus;

    public DataMember() {

    }

    public DataMember(@NotNull Member member, @NotNull DataGroup dataGroup) {
        this.demander = (Demander) member.getDemander();
        this.addDemanderBonus = member.getAddDemanderBonus();
        this.dataGroup = dataGroup;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (id == null) return false;
        if (!(obj instanceof DataMember)) return false;
        IdModel idModel = (IdModel) obj;
        return id.equals(idModel.getId());
    }

    @Override
    public DataGroup getGroup() {
        return getDataGroup();
    }

    @Override
    public double getDemanderOpinionValue(Offer offer) {
        return demander.getOpinionValue(offer);
    }
}
