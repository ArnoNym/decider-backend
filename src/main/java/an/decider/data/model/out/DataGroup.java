package an.decider.data.model.out;

import an.decider.calc.methods.Opinions;
import an.decider.calc.output.Group;
import an.decider.data.model.IdModel;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
public class DataGroup implements IdModel, Group {
    @Id
    private String id;

    @ManyToOne
    private DataChoice dataChoice;

    @ManyToOne
    private Offer offer;

    @OneToMany(mappedBy = "dataGroup", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DataMember> dataMemberList = new ArrayList<>();

    /*
    @Nullable
    @Transient
    private an.decider.calc.Group group = null;
    */

    public DataGroup() {
    }

    /**
     * Constructor needed for a Query in
     * @see an.decider.data.dao.DataGroupDao#findAllOfferMinAndMaxGroupSizeByDataGroupIds(Collection)
     */
    public DataGroup(String id, int minGroupSize, Integer maxGroupSize) {
        this.id = id;
        this.offer = new Offer();
        offer.setMinGroupSize(minGroupSize);
        offer.setMaxGroupSize(maxGroupSize);
    }

    public DataGroup(DataChoice dataChoice, Offer offer) {
        this.dataChoice = dataChoice;
        this.offer = offer;
    }

    public DataGroup(@NotNull Offer offer, @NotNull List<DataMember> dataMemberList) {
        this.offer = offer;
        this.dataMemberList = dataMemberList;
    }

    @Override
    public double calcUtility() {
        if (dataMemberList == null) return 0;
        return Group.super.calcUtility();
    }

    public void validate() {
        if (dataMemberList.isEmpty()) throw new IllegalArgumentException("demanderList may not be empty");
        /*for (Demander demander : dataMemberList) {
            demander.validate();
        }*/
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (id == null) return false;
        if (!(obj instanceof DataGroup)) return false;
        IdModel idModel = (IdModel) obj;
        return id.equals(idModel.getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(offer.getName()).append(": ");
        for (DataMember dataMember : dataMemberList) {
            Demander demander = dataMember.getDemander();
            sb.append(demander.getName())
                    .append("(")
                    //.append(demander.getRank(offer))
                    .append(",")
                    //.append(demander.getOpinionValue(offer))
                    .append(")")
                    .append(", ");
        }
        return sb.toString();
    }

    @Transient
    private int showSpaceCount = -1;
    public int calcShowSpacesCount() {
        return calcShowSpacesCount(dataChoice.calcDemanderCount());
    }
    public int calcShowSpacesCount(int memberCount) {
        if (showSpaceCount == -1) {
            Optional<Integer> maxGroupSizeOptional = offer.getMaxGroupSizeOptional();
            showSpaceCount = maxGroupSizeOptional.map(maxGroupSize -> Math.min(memberCount, maxGroupSize)).orElse(memberCount);
        }
        return showSpaceCount;
    }

    public int getDataMemberListSize() {
        if (dataMemberList == null) {
            return 0;
        } else {
            return dataMemberList.size();
        }
    }

    public boolean fulFillsMinGroupSizeConstraint() {
        return getDataMemberListSize() >= offer.getMinGroupSize();
    }

    public boolean fullFillsMaxGroupSizeConstraint() {
        Optional<Integer> maxGroupSizeOptional = offer.getMaxGroupSizeOptional();
        if (maxGroupSizeOptional.isEmpty()) return true;
        return getDataMemberListSize() <= maxGroupSizeOptional.get();
    }

    @Override
    public List<DataMember> getMembers() {
        return getDataMemberList();
    }
}
