package an.decider.data.model;

import an.decider.data.DataUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface IdModel {
    String getId();
    void setId(String id);

    /*
    @NotNull
    default String setGeneratedId(@NotNull Collection<Long> inUseIds) {
        return DataUtils.setGeneratedId(this, inUseIds);
    }+
            */
}
