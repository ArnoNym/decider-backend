package an.decider.data.model.input;

import an.decider.data.model.IdModel;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
public class Opinion implements IdModel, IndexModel, an.decider.calc.input.Opinion {
    @Size(min = 1)
    @Id
    private String id;

    @NotNull
    @ManyToOne //TODO Pruefe ob fetching type lazy eine gute Idee ist
    private Demander demander;

    @NotNull
    @ManyToOne
    private Offer offer;

    @Min(-1)
    private Double value;

    private String note = null;

    @Transient
    private Integer index = null;

    public Opinion() {
    }

    public Opinion(String id, Demander demander, Offer offer, Double value) {
        this.id = id;
        this.demander = demander;
        this.offer = offer;
        this.value = value;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Opinion)) return false;
        Opinion other = (Opinion) obj;
        return id.equals(other.id);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(id).append(", ");
        sb.append("index=").append(index).append(", ");
        if (demander == null) {
            sb.append("demander == null").append(", ");
        } else {
            sb.append("demander.id =").append(demander.getId()).append(", ");
            sb.append("demander.index =").append(demander.getIndex()).append(", ");
        }
        if (offer == null) {
            sb.append("offer == null").append(", ");
        } else {
            sb.append("offer.id =").append(offer.getId()).append(", ");
            sb.append("offer.index =").append(offer.getIndex()).append(", ");
        }
        sb.append("value=").append(value).append(", ");
        sb.append("note=").append(note);
        return sb.toString();
    }
}
