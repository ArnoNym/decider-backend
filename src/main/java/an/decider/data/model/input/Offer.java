package an.decider.data.model.input;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Offer implements DoModel, an.decider.calc.input.Offer {
    @Id
    private String id;

    @ManyToOne
    private Decision decision;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean hasToBeIncluded = false;

    private Integer available = 1;

    @Column(nullable = false)
    private Integer minGroupSize = 0;

    private Integer maxGroupSize = null;

    private String note = null;

    @OneToMany(mappedBy = "offer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Opinion> opinionList = new ArrayList<>();

    @Transient
    private Integer index = null;

    public Offer() {
    }

    public Offer(String id, String name, boolean hasToBeIncluded, Integer available, int minGroupSize, Integer maxGroupSize) {
        this.id = id;
        this.name = name;
        this.hasToBeIncluded = hasToBeIncluded;
        this.available = available;
        this.minGroupSize = minGroupSize;
        this.maxGroupSize = maxGroupSize;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Offer)) return false;
        Offer offer = (Offer) obj;
        return id.equals(offer.id);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(id).append(", ");
        sb.append("index=").append(index).append(", ");
        if (decision == null) {
            sb.append("decision=null").append(", ");
        } else {
            sb.append("decisionId=").append(decision.getId()).append(", ");
        }
        sb.append("name=").append(name).append(", ");
        sb.append("hasToBeIncluded=").append(hasToBeIncluded).append(", ");
        sb.append("available=").append(available).append(", ");
        sb.append("minGroupSize=").append(minGroupSize).append(", ");
        sb.append("maxGroupSize=").append(maxGroupSize).append(", ");
        int i = 0;
        for (Opinion opinion : opinionList) {
            sb.append("Opinion nr ").append(i).append(": ").append(opinion.toString());
            i++;
        }
        return sb.toString();
    }
}
