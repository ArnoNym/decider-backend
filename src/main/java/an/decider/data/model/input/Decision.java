package an.decider.data.model.input;

import an.decider.data.model.IdModel;
import an.decider.services.Utils;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Data
public class Decision implements IdModel, an.decider.calc.input.Decision {
    @Id
    private String id = Utils.generateUUID();

    @Column(nullable = false, unique = true)
    private String inviteId = Utils.generateUUID();

    @Column(nullable = false)
    private Long version = 0L;

    @Column(nullable = false)
    private Timestamp createdTime = new Timestamp((new Date()).getTime());

    @Column(nullable = false)
    private Timestamp lastUsedTime = createdTime;

    @Column(nullable = false)
    private String name;

    private String password = null;

    @Column(nullable = false)
    private Integer maxOpinionValue = 10;

    private String note = null;

    @OneToMany(mappedBy = "decision", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Offer> offerList = new ArrayList<>();

    @OneToMany(mappedBy = "decision", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Demander> demanderList = new ArrayList<>();

    @Column(nullable = false)
    private Boolean offerLinkActive = false;

    @Column(nullable = false)
    private Boolean demanderLinkActive = false;

    @Column(nullable = false)
    private Double addDemanderBonus = 10d;

    public Decision() {
    }

    public Decision(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Decision)) return false;
        IdModel idModel = (IdModel) obj;
        return id.equals(idModel.getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Offer offer : offerList) {
            sb.append("Offer nr ").append(i).append("\n");
            i++;
            sb.append(offer.toString()).append("\n");
        }
        sb.append("\n");
        i = 0;
        for (Demander demander : demanderList) {
            sb.append("Demander nr ").append(i).append("\n");
            i++;
            sb.append(demander.toString()).append("\n");
        }
        return sb.toString();
    }
}
