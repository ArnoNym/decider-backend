package an.decider.data.model.input;

import an.decider.data.model.IdModel;

import java.util.List;

public interface DoModel extends IdModel, IndexModel {
    Decision getDecision();
    void setDecision(Decision decision);
    String getName();
    void setName(String name);
    Boolean getHasToBeIncluded();
    void setHasToBeIncluded(Boolean hasToBeIncluded);
    List<Opinion> getOpinionList();
    void setOpinionList(List<Opinion> opinionList);
}
