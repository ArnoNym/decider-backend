package an.decider.data.model.input;

import an.decider.data.model.IdModel;

public interface IndexModel extends IdModel {
    void setIndex(Integer index);
    Integer getIndex();

    default boolean requiresId() {
        String id = getId();
        Integer index = getIndex();
        return (id == null || id.equals("")) && index != null;
    }

    default boolean shouldGetDeleted() {
        String id = getId();
        Integer index = getIndex();
        return (id == null || id.equals("")) && index == null;
    }
}
