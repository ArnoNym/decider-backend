package an.decider.data.model.input;

import an.decider.services.Utils;

import javax.persistence.*;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

public class Invite {
    @Id
    private String id;

    private String name = null;

    private Integer remainingUses = null;

    @Column(nullable = false)
    private Instant creationInstant;

    private Duration timeToLive = null;

    @Column(nullable = false)
    private Boolean activ;

    @ManyToOne
    private Decision decision;

    public Invite() {
    }

    public void init() {
        this.id = Utils.generateUUID();
        this.creationInstant = Instant.now();
    }

    public boolean isActiv() {
        if (timeToLive != null) {
            Instant expirationInstant = creationInstant.plus(timeToLive);
            if (expirationInstant.isBefore(Instant.now())) {
                return false;
            }
        }

        // ...

        return true;
    }

    // get and set =====================================================================================================

}
