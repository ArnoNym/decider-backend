package an.decider.data.model.input;

import an.decider.calc.input.Offer;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Data
public class Demander implements DoModel, an.decider.calc.input.Demander {
    @Size(min = 1)
    @Id
    private String id;

    @NotNull
    @ManyToOne
    private Decision decision;

    @NotNull
    @Column(nullable = false)
    private String name;

    @NotNull
    private Boolean hasToBeIncluded = false;

    @OneToMany(mappedBy = "demander", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Opinion> opinionList = new ArrayList<>();

    @Transient
    private Integer index = null;

    @Transient
    private Map<Offer, Double> offerOpinionMap = null;

    public Demander() {
    }

    public double getOpinionValue(Offer offer) {
        if (offer == null) {
            throw new IllegalArgumentException();
        }
        for (Opinion opinion : opinionList) {
            if (offer.equals(opinion.getOffer())) {
                return opinion.getValue();
            }
        }
        throw new IllegalArgumentException();
    }

    /*
    @Contract(pure = true)
    public double getOpinionValue(@NotNull String offerId) {
        Double opinion = an.decider.calc.base.Demander.super.getOpinionValue(offerId);
        if (Const.DEVELOPER && opinion == null) {
            if (opinionList.isEmpty()) {
                throw new IllegalStateException("opinionList.isEmpty()!");
            } else {
                throw new IllegalStateException();
            }
        }
        return opinion;
    }
    */

    /*
    @Contract(pure = true)
    public double getRank(@NotNull Offer offer) {
        return getRank(offer.getId());
    }

    @Contract(pure = true)
    public double getRank(@NotNull String offerId) {
        double opinion = getOpinionValue(offerId);
        int rank = 1;
        for (double op : Opinions.offerIdOpinionMap(getId(), getOpinionList()).values()) {
            if (op > opinion) {
                rank++;
            }
        }
        return rank;
    }
    */

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Demander)) return false;
        Demander demander = (Demander) obj;
        return id.equals(demander.id);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(id).append(", ");
        sb.append("index=").append(index).append(", ");
        if (decision == null) {
            sb.append("decision=null").append(", ");
        } else {
            sb.append("decisionId=").append(decision.getId()).append(", ");
        }
        sb.append("name=").append(name).append(", ");
        sb.append("hasToBeIncluded=").append(hasToBeIncluded).append(", ");
        int i = 0;
        for (Opinion opinion : opinionList) {
            sb.append("\n").append("Opinion nr ").append(i).append(": ").append(opinion.toString());
            i++;
        }
        return sb.toString();
    }

    @Override
    public List<Opinion> getOpinions() {
        return getOpinionList();
    }
}
