package an.decider.data.dao;

import an.decider.data.model.input.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Repository
public interface DecisionDao extends CrudRepository<Decision, String> {
    @Query(value = "SELECT d.id " +
            "FROM decision d " +
            "WHERE d.invite_id = ?1",
            nativeQuery = true
    )
    String findIdByInviteId(String inviteId);

    @Query(value = "SELECT d.version " +
            "FROM decision d " +
            "WHERE d.id = ?1",
            nativeQuery = true
    )
    Long findVersion(String decisionId);

    @Query(value = "SELECT d.name FROM decision d, data_choice c WHERE d.id = c.decision_id AND c.id = ?1", nativeQuery = true)
    String findByChoiceId(String choiceId);

    @Query(value = "SELECT d.Offer_Link_Active FROM decision d WHERE d.id = ?1", nativeQuery = true)
    Boolean isOfferLinkActiveByDecisionId(String decisionId);

    @Query(value = "SELECT d.Offer_Link_Active FROM decision d WHERE d.invite_id = ?1", nativeQuery = true)
    Boolean isOfferLinkActiveByInviteId(String inviteId);

    @Query(value = "SELECT d.Demander_Link_Active FROM decision d WHERE d.id = ?1", nativeQuery = true)
    Boolean isDemanderLinkActiveByDecisionId(String decisionId);

    @Query(value = "SELECT d.Demander_Link_Active FROM decision d WHERE d.invite_id = ?1", nativeQuery = true)
    Boolean isDemanderLinkActiveByInviteId(String inviteId);

    @Query(value = "SELECT d.invite_id FROM decision d WHERE d.id = ?1", nativeQuery = true)
    String findInviteIdById(String decisionId);

    @Query(value = "SELECT d.name FROM decision d WHERE d.id = ?1", nativeQuery = true)
    String findNameById(String decisionId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE decision " +
            "SET name = :name, add_demander_bonus = :addDemanderBonus, max_opinion_value = :maxOpinionValue " +
            "WHERE id = :id",
            nativeQuery = true
    )
    void updateSettings(@Param("id") String id,
                        @Param("name") String name,
                        @Param("addDemanderBonus") double addDemanderBonus,
                        @Param("maxOpinionValue") int maxOpinionValue);

    @Modifying
    @Transactional
    @Query(value = "UPDATE decision " +
            "SET Demander_Link_Active = :demanderLinkActive, Offer_Link_Active = :offerLinkActive " +
            "WHERE id = :id",
            nativeQuery = true
    )
    void updateInvite(@Param("id") String id,
                      @Param("demanderLinkActive") Boolean demanderLinkActive,
                      @Param("offerLinkActive") Boolean offerLinkActive);

    @Query(value = "SELECT d.name " +
            "FROM decision d " +
            "WHERE d.id = ?1",
            nativeQuery = true
    )
    String findDecisionNameByDecisionId(String decisionId);

    @Query(value = "SELECT d.max_opinion_value " +
            "FROM decision d " +
            "WHERE d.id = ?1",
            nativeQuery = true
    )
    double findMaxOpinionValue(String decisionId);
}
