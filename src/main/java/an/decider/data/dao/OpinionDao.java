package an.decider.data.dao;

import an.decider.data.model.input.Opinion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public interface OpinionDao extends CrudRepository<Opinion, String> {
    @Query(
            value = "SELECT o.demander_Id, o.offer_Id, o.value, o.note " +
                    "FROM Opinion AS o, Demander AS d " +
                    "WHERE o.demander_Id = d.id AND d.decision_Id = ?1",
            nativeQuery = true
    )
    List<Opinion> findAllByDecisionId(@RequestParam("decisionId") String decisionId);

    @Query(
            value = "SELECT o.demander_Id, o.offer_Id, o.value, o.note " +
                    "FROM Opinion AS o, Demander AS d " +
                    "WHERE o.demander_Id = d.id " +
                    "AND d.decision_Id = ?1 " +
                    "AND o.demander_Id = ?2",
            nativeQuery = true
    )
    List<Opinion> findAllByDecisionIdAAndDemanderId(@RequestParam("decisionId") String decisionId, @RequestParam("demanderId") String demanderId);
}
