package an.decider.data.dao;

import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface DemanderDao extends CrudRepository<Demander, String> {

    List<Demander> findAllByDecisionId(String decisionId);

    @Transactional
    List<Demander> deleteAllByDecisionId(String decisionId);

    @Transactional
    List<Demander> deleteAllByIdNotIn(Collection<String> demanderIds);

    Set<Demander> findByDecisionIdAndIdNotIn(String decisionId, Collection<String> ids);
}
