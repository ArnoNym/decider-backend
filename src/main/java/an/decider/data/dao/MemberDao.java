package an.decider.data.dao;

import an.decider.data.model.out.DataGroup;
import an.decider.data.model.out.DataMember;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface MemberDao extends CrudRepository<DataMember, String> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE data_member SET data_group_id = :dataGroupId WHERE id = :id", nativeQuery = true)
    void saveMinimal(@Param("id") String id, @Param("dataGroupId") String dataGroupId);

    default void saveAllMinimal(Collection<DataMember> dataMembers) {
        for (DataMember dataMember : dataMembers) {
            saveMinimal(dataMember.getId(), dataMember.getDataGroup().getId());
        }
    }

    List<DataMember> findAllByDataGroupId(String dataGroupId);
}
