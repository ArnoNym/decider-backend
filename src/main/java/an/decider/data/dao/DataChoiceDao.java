package an.decider.data.dao;

import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.out.DataChoice;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface DataChoiceDao extends CrudRepository<DataChoice, String> {

    //@Modifying
    @Transactional
    //@Query(value = "DELETE FROM data_choice WHERE decision_id = ?1", nativeQuery = true)
    void deleteAllByDecisionId(String decisionId);

    List<DataChoice> findAllByDecisionId(String decisionId);

    @Query(value = "SELECT modified FROM data_choice WHERE id = ?1", nativeQuery = true)
    Boolean findModifiedById(String choiceId);
}
