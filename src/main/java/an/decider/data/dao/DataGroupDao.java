package an.decider.data.dao;

import an.decider.data.model.out.DataGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface DataGroupDao extends CrudRepository<DataGroup, String> {

    @Query(
            value = "SELECT dg " +
                    "FROM DataGroup dg, DataMember dm " +
                    "WHERE dg.id = dm.dataGroup.id AND dm.id = ?1"
    )
    DataGroup findByDataMemberId(String dataMemberId);

    @Query(
            value = "SELECT new DataGroup(g.id, o.minGroupSize, o.maxGroupSize) " +
                    "FROM DataGroup g, Offer o " +
                    "WHERE g.offer.id = o.id AND g.id IN ?1"
    )
    List<DataGroup> findAllOfferMinAndMaxGroupSizeByDataGroupIds(Collection<String> dataGroupId);

    List<DataGroup> findAllByDataChoiceId(String choiceId);

    @Transactional
    void removeAllByDataChoiceId(String dataChoiceId);
}
