package an.decider.data.dao;

import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Offer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Max;
import java.beans.Transient;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface OfferDao extends CrudRepository<Offer, String> {
    List<Offer> findAllByDecisionId(String decisionId);

    @Query(
        value = "SELECT Id " +
                "FROM Offer " +
                "WHERE Id" +
                "NOT IN (SELECT offer_Id FROM Opinion WHERE demander_Id = ?1)",
        nativeQuery = true
    )
    List<String> findAllOfferIdsNotInOpinionForDemander(@RequestParam("demanderId") String demanderId);

    @Transactional
    List<Offer> deleteAllByIdNotIn(Collection<String> offerIds);

    @Transactional
    List<Offer> deleteAllByDecisionId(String decisionId);
}
