package an.decider.data.validator;

import an.decider.data.model.input.Offer;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import java.util.Collection;

@Service
public class OfferValidator {

    public void validate(Offer offer) throws ValidationException {
        if (offer.getId() == null) throw new ValidationException();

        if (offer.getName() == null) throw new ValidationException();
        if (offer.getName().equals("")) throw new ValidationException();

        if (offer.getHasToBeIncluded() == null) throw new ValidationException();

        if (offer.getAvailable() != null && offer.getAvailable() <= 0) throw new ValidationException();
        if (offer.getAvailable() == null && offer.getHasToBeIncluded()) throw new ValidationException();

        if (offer.getMinGroupSize() == null) throw new ValidationException();
        if (offer.getMinGroupSize() < 0) throw new ValidationException();
        if (offer.getMaxGroupSize() != null && offer.getMaxGroupSize() < offer.getMinGroupSize()) throw new ValidationException();
    }
}
