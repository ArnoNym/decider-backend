package an.decider.data.validator;

import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import org.springframework.stereotype.Service;

@Service
public class DemanderValidator {
    public void validate(Demander demander) throws ValidationException {
        if (demander.getId() == null) throw new ValidationException();

        if (demander.getName() == null) throw new ValidationException();
        if (demander.getName().equals("")) throw new ValidationException();

        if (demander.getHasToBeIncluded() == null) throw new ValidationException();
    }
}
