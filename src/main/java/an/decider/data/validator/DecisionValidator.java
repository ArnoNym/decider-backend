package an.decider.data.validator;

import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;

public class DecisionValidator {
    public static void validate(Decision decision) throws ValidationException {
        if (decision.getId().equals("")) throw new IllegalArgumentException();
        if (decision.getName().equals("")) throw new IllegalArgumentException();

        if (decision.getMaxOpinionValue() == null) throw new ValidationException();
        if (decision.getMaxOpinionValue() < 1) throw new ValidationException(); //TODO Fehlermeldung "maxOpinionValue must be greater than or equal to 1. maxOpinionValue="+decision.getMaxOpinionValue()

        if (decision.getAddDemanderBonus() == null) throw new ValidationException();
        if (decision.getAddDemanderBonus() < 0) throw new ValidationException();
    }
}
