package an.decider.data.validator;

import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Offer;
import an.decider.data.model.input.Opinion;

import java.util.Collection;

public class OpinionValidator {
    public void validate(Decision decision) throws ValidationException {
        // todo
    }

    public void validate(Collection<Opinion> opinions, Integer maxOpinionValue) throws ValidationException {
        // todo
    }

    public void validate(Opinion opinion, Integer maxOpinionValue) throws ValidationException {
        if (opinion.getId() == null) throw new ValidationException();
        //todo
    }
}
