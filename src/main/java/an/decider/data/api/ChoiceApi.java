package an.decider.data.api;

import an.decider.data.dao.DataChoiceDao;
import an.decider.data.model.out.DataChoice;
import an.decider.data.model.out.DataMember;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ChoiceApi {
    private final DataChoiceDao dataChoiceDao;

    public List<DataChoice> findAllByDecisionId(String decisionId) {
        List<DataChoice> dataChoiceList = dataChoiceDao.findAllByDecisionId(decisionId);
        for (DataChoice dataChoice : dataChoiceList) {
            setAddDemanderBonus(dataChoice);
        }
        return dataChoiceList;
    }

    public DataChoice findById(String id) {
        DataChoice dataChoice = dataChoiceDao.findById(id).orElseThrow();
        setAddDemanderBonus(dataChoice);
        return dataChoice;
    }

    private void setAddDemanderBonus(DataChoice dataChoice) {
        Double addDemanderBonus = dataChoice.getDecision().getAddDemanderBonus();
        for (DataMember dataMember : dataChoice.calcMemberList()) {
            dataMember.setAddDemanderBonus(addDemanderBonus);
        }
    }

    public void deleteAllByDecisionId(String decisionId) {
        dataChoiceDao.deleteAllByDecisionId(decisionId);
    }

    public Boolean findModifiedById(String choiceId) {
        return dataChoiceDao.findModifiedById(choiceId);
    }

    public void saveAll(String decisionId, List<DataChoice> dataChoiceList) {
        deleteAllByDecisionId(decisionId);
        dataChoiceDao.saveAll(dataChoiceList);
    }

    public void save(DataChoice dataChoice) {
        dataChoiceDao.save(dataChoice);
    }
}
