package an.decider.data.api;

import an.decider.data.dao.DemanderDao;
import an.decider.data.model.input.Demander;
import an.decider.data.validator.DemanderValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DemanderApi {
    private final DemanderDao demanderDao;
    private final DemanderValidator demanderValidator;

    public void save(Demander demander) {
        demanderValidator.validate(demander);
        // todo validate opinions
        demanderDao.save(demander);
    }

    public void setLastUsedTime(String decisionId) {
        // todo
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<Demander> findAll(String decisionId) {
        return demanderDao.findAllByDecisionId(decisionId);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveAll(Collection<Demander> demanders) {
        demanderDao.saveAll(demanders);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<Demander> deleteAllByIdNotIn(String decisionId, Collection<String> demanderIds) {
        if (demanderIds.isEmpty()) {
            demanderDao.deleteAllByDecisionId(decisionId);
        }
        return demanderDao.deleteAllByIdNotIn(demanderIds);
    }
}
