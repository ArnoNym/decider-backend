package an.decider.data.api;

import an.decider.data.dao.DecisionDao;
import an.decider.data.model.input.Decision;
import an.decider.data.validator.DecisionValidator;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DecisionApi {
    private final DecisionDao decisionDao;

    public boolean isCurrentVersion(String decisionId, long version) {
        long repoVersion = decisionDao.findVersion(decisionId);
        if (repoVersion < version) throw new IllegalArgumentException();
        return repoVersion == version;
    }

    public void save(Decision decision) {
        decisionDao.save(decision);
    }

    public String findDecisionNameByDecisionId(String decisionId) {
        return decisionDao.findDecisionNameByDecisionId(decisionId);
    }

    public void updateInvite(String id, Boolean demanderLinkActive, Boolean offerLinkActive) {
        decisionDao.updateInvite(id, demanderLinkActive, offerLinkActive);
    }

    public Decision findById(String id) {
        return decisionDao.findById(id).orElseThrow();
    }

    public String findNameByChoiceId(String choiceId) {
        return decisionDao.findByChoiceId(choiceId);
    }

    public String findIdByInviteId(String inviteId) {
        return decisionDao.findIdByInviteId(inviteId);
    }

    public void updateSettings(Decision decision) {
        DecisionValidator.validate(decision);
        decisionDao.updateSettings(decision.getId(), decision.getName(), decision.getAddDemanderBonus(), decision.getMaxOpinionValue());
    }

    public double getMaxOpinionValue(String decisionId) {
        return decisionDao.findMaxOpinionValue(decisionId);
    }

    public boolean isOfferLinkActiveById(String decisionId) {
        Boolean a = decisionDao.isOfferLinkActiveByDecisionId(decisionId);
        if (a == null) {
            throw new IllegalStateException();
        }
        return a;
    }
    public boolean isOfferLinkActiveByInviteId(String inviteId) {
        if (inviteId == null) throw new IllegalArgumentException();
        return decisionDao.isOfferLinkActiveByInviteId(inviteId);
    }

    public boolean isDemanderLinkActiveById(String inviteId) {
        String decisionId = decisionDao.findIdByInviteId(inviteId);
        Boolean a = decisionDao.isDemanderLinkActiveByDecisionId(decisionId);
        if (a == null) {
            throw new IllegalStateException();
        }
        return a;
    }
    public boolean isDemanderLinkActiveByInviteId(String inviteId) {
        if (inviteId == null) throw new IllegalArgumentException();
        return decisionDao.isDemanderLinkActiveByInviteId(inviteId);
    }

    public String findInviteId(String decisionId) {
        return decisionDao.findInviteIdById(decisionId);
    }

    public String findName(String decisionId) {
        return decisionDao.findNameById(decisionId);
    }
}
