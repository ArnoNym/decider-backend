package an.decider.data.api;

import an.decider.data.dao.OfferDao;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import an.decider.data.validator.OfferValidator;
import an.decider.data.validator.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OfferApi {
    private final OfferDao offerDao;
    private final OfferValidator offerValidator;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<Offer> findAll(String decisionId) {
        return offerDao.findAllByDecisionId(decisionId);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveAll(Collection<Offer> offers) throws ValidationException {
        for (Offer offer : offers) offerValidator.validate(offer);
        offerDao.saveAll(offers);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<Offer> deleteAllByIdNotIn(String decisionId, Collection<String> offerIds) {
        if (offerIds.isEmpty()) {
            offerDao.deleteAllByDecisionId(decisionId);
        }
        return offerDao.deleteAllByIdNotIn(offerIds);
    }
}
