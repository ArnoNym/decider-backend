package an.decider.utils.iterator;

import java.util.Collection;
import java.util.Iterator;

public class EndlessIterator<E> implements Iterator<E> {
    private Collection<E> collection;
    private Iterator<E> iterator;

    public EndlessIterator(Collection<E> collection) {
        this.collection = collection;
        this.iterator = collection.iterator();
    }

    @Override
    public void remove() {
        iterator.remove();
    }

    @Override
    public boolean hasNext() {
        return !collection.isEmpty();
    }

    @Override
    public E next() {
        if (!iterator.hasNext()) {
            if (collection.isEmpty()) {
                throw new IllegalStateException();
            }
            iterator = collection.iterator();
        }
        return iterator.next();
    }
}
