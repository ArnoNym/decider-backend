package an.decider.utils.iterator;

import an.decider.Const;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class RoundsIterator<E> implements Iterator<E> {
    @Nullable
    private final Collection<E> collection;
    @NotNull
    private final List<E> list;
    @NotNull
    private Iterator<E> iterator;
    @Nullable
    private Integer countdown = null;
    private E currentElement = null;

    public RoundsIterator(Collection<E> collection) {
        if (collection instanceof List) {
            this.collection = null;
            this.list = (List<E>) collection;
        } else {
            this.collection = collection;
            this.list = new LinkedList<>(collection);
        }
        newIterator();
    }

    @Override
    public void remove() {
        if (collection != null) {
            collection.remove(currentElement);
        }
        iterator.remove();
    }

    private void newIterator() {
        if (Const.DEVELOPER && list.isEmpty()) {
            throw new IllegalStateException();
        }
        iterator = list.iterator();
    }

    @Override
    public boolean hasNext() {
        return (countdown == null || countdown > 0) && (iterator.hasNext() || !list.isEmpty());
    }

    public boolean setOneLastRound() {
        int size = list.size();
        if (countdown != null && countdown == size) return false;
        countdown = size;
        return true;
    }

    public boolean removeOneLastRound() {
        if (countdown == null) return false;
        countdown = null;
        return true;
    }

    @Override
    public E next() {
        if (!hasNext()) {
            throw new IllegalStateException("No next Element!");
        }

        if (!iterator.hasNext()) {
            newIterator();
        }

        if (countdown != null) {
            countdown--;
        }
        currentElement = iterator.next();
        return currentElement;
    }
}
