package an.decider.utils;

import java.util.*;

public class SetUtils {

    public static <O> Set<List<O>> createPowerSet(List<O> objects, int minSetSize, Integer maxSetSize) {
        Set<List<O>> powerSet = new HashSet<>((int) Math.pow(2, objects.size()) - 1);
        fillPowerSet(powerSet, objects, minSetSize, maxSetSize);
        return powerSet;
    }

    public static <O> void fillPowerSet(Set<List<O>> powerSet, List<O> objects, int minSetSize, Integer maxSetSize) {
        int size = objects.size();

        if (size < minSetSize) {
            return;
        }

        if (maxSetSize == null || size <= maxSetSize) {
            if (!powerSet.add(objects)) {
                return;
            }
        }

        for (int i = 0; i < size; i++) {
            List<O> objectsMinusOne = new LinkedList<>();//objects.size() - 1
            int j = 0;
            for (O object : objects) {
                if (j != i) {
                    objectsMinusOne.add(object);
                }
                j++;
            }
            fillPowerSet(powerSet, objectsMinusOne, minSetSize, maxSetSize);
        }
    }
}
