package an.decider.calc;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import an.decider.calc.input.Opinion;
import an.decider.calc.methods.Groups;
import an.decider.calc.methods.Members;
import an.decider.utils.iterator.EndlessIterator;
import an.decider.utils.iterator.RoundsIterator;
import an.decider.Const;
import org.jetbrains.annotations.NotNull;
import org.springframework.expression.spel.ast.OpInc;

public class Choice {
    private final Collection<Member> members;
    private final Collection<Group> groups;

    public Choice(double demanderAddBonus,
                  @NotNull Collection<? extends Demander> demanders,
                  @NotNull Collection<? extends Offer> offerList) {
        this.groups = Groups.createGroups(offerList);
        this.members = Members.createMembers(demanderAddBonus, demanders);
    }

    public boolean optimize() {
        assert !groups.isEmpty();

        if (groups.size() == 1) {
            Group group = groups.iterator().next();
            for (Member member : members) {
                member.joinGroup(group);
            }
            return !group.isEmpty();
        }

        if (!maximizeUtility(members, groups)) return false;

        return isEveryDemanderThatHasToBeIncludedInAGroup();
    }

    private boolean maximizeUtility(Collection<Member> members, @NotNull Collection<Group> groups) {
        boolean changed = false;
        RoundsIterator<Member> memberRoundsIterator = new RoundsIterator<>(new LinkedList<>(members));
        memberRoundsIterator.setOneLastRound();
        while (memberRoundsIterator.hasNext()) {
            Member member = memberRoundsIterator.next();
            /* Join noetig, da durch Begging Gruppen attraktiver werden, die vorher keine
            Mitglieder hatten */
            if (member.joinBestGroup(groups) | member.switchGroupsIfAddUtility(members, groups)) {
                changed = true;
                memberRoundsIterator.setOneLastRound();
            }
        }
        return changed;
    }

    public double calcUtility() {
        double utility = 0;
        for (Group group : groups) {
            utility += group.calcUtility();
        }
        return utility;
    }
/*
    private boolean distributeHasToBeIncludedMembers() {
        Set<Member> hasToBeIncludedMembers = members.stream().filter(Member::insistsOnGroup).collect(Collectors.toSet());
        if (hasToBeIncludedMembers.isEmpty()) return true;
        return maximizeUtility(hasToBeIncludedMembers);
    }

    **
     * fulfill min-group-size-constraints: So umstaendlich, da durch das joinen schon als 'full' betitelte
     * Gruppen wieder platz bekommen koennen
     *
    private void fulfillMinGroupSizeConstraints() {
        RoundsIterator<Group> groupRoundsIterator = new RoundsIterator<>(groups);
        groupRoundsIterator.setOneLastRound();
        while (groupRoundsIterator.hasNext()) {
            Group group = groupRoundsIterator.next();
            if (!group.fulfillsMinGroupSizeConstraint()) {
                do {
                    group.incrementBegging();
                } while (!maximizeUtility());
                groupRoundsIterator.setOneLastRound();
            }
        }
        for (Group group : groups) {
            group.deleteBegging();
        }
    }

    private void evenOutGroups() {
        if (members.size() < 2) return;
        if (groups.isEmpty()) throw new IllegalArgumentException("Groups Collection may not be empty!");

        int goalCount = members.size();
        int count = 0;

        Iterator<Member> memberIterator = new EndlessIterator<>(new LinkedList<>(members));
        while (count < goalCount) {
            boolean removeMember = false;
            Member member = memberIterator.next();
            Optional<Group> memberGroupOptional = member.getGroupOptional();

            if (memberGroupOptional.isPresent()) {
                Group memberGroup = memberGroupOptional.get();

                if (memberGroup.size() > memberGroup.getMinGroupSize()) {
                    for (Group group : groups) {
                        if (group.size() < group.getMaxGroupSize()
                                && group.size() < memberGroup.size()
                                && member.isEqualUtilityGroup(group)
                                && member.joinGroup(group)) {
                            removeMember = true;
                            break;
                        }
                    }
                }
            } else if (Const.DEVELOPER && member.insistsOnGroup()) {
                throw new IllegalStateException();
            }

            if (removeMember) {
                memberIterator.remove();
                goalCount--;
                count = 0;
            } else {
                count++;
            }
        }
    }
    */

    /*
    private static void minimizeBegging(@NotNull Collection<Group> groups) {
        RoundsIterator<Group> groupRoundsIterator = new RoundsIterator<>(groups);
        groupRoundsIterator.setOneLastRound();

        while (groupRoundsIterator.hasNext()) {
            Group group = groupRoundsIterator.next();

            if (Const.DEVELOPER) {
                for (Member member : group.getMembers()) {
                    if (member.hasJoins(groups, false, false, true)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("\n");
                        for (Group g : groups) {
                            sb.append("GroupId=").append(g.getOffer().getId()).append(", ")
                                    .append("GroupName=").append(g.getOffer().getName()).append("\n");
                            sb.append("Utility=").append(g.calcUtility()).append("\n");
                            sb.append("Begging=").append(g.getBegging()).append("\n");;
                            sb.append("MemberCount=").append(g.getMembers().size()).append("\n");
                            sb.append("\n");
                        }
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
            }

            boolean decrementedMinOnce = false;
            boolean positiveUtilityJoins = false;
            while (!positiveUtilityJoins && !group.isBeggingZero()) {
                group.decrementBegging();

                for (Member member : group.getMembers()) {
                    if (member.hasJoins(groups, false, false, true)) {
                        positiveUtilityJoins = true;
                        group.incrementBegging();
                        break;
                    }
                }

                if (!positiveUtilityJoins) {
                    decrementedMinOnce = true;
                }
            }

            if (decrementedMinOnce) {
                groupRoundsIterator.setOneLastRound();
            }
        }
    }
    */

    // Tests ===========================================================================================================

    // Requires information from the whole Decision

    public static boolean isEveryRequiredOfferIncluded(int offerHasToBeIncludedCount, Collection<? extends Offer> offers) {
        int thisListOfferHasToBeIncludedCount = (int) offers.stream().filter(Offer::getHasToBeIncluded).count();
        return thisListOfferHasToBeIncludedCount >= offerHasToBeIncludedCount;
    }

    // Requires only information that are available after creating the Choice

    public static boolean isEveryOfferAbleToFulfillTheirMinSizeConstraint(Collection<? extends Demander> demanders,
                                                                          Collection<? extends Offer> offers) {
        int minGroupSizeSum = offers.stream().mapToInt(Offer::getMinGroupSize).sum();
        return minGroupSizeSum <= demanders.size();
    }

    /**
     * This test is not sufficent. There may be a Group with a maximum of one Member but 2 Demanders that gave every
     * Group but this one a negative rating
     */
    public static boolean isEveryDemanderThatHasToBeIncludedAbleToFindAGroup_light(Collection<? extends Demander> demanders,
                                                                                   Collection<? extends Offer> offers) {
        Integer maxGroupSizeSum = 0;
        for (Offer offer : offers) {
            if (offer.getMaxGroupSizeOptional().isEmpty()) {
                maxGroupSizeSum = null;
                break;
            }
            maxGroupSizeSum += offer.getMaxGroupSize();
        }

        if (maxGroupSizeSum == null) {
            return true;
        }

        long demanderHasToBeIncludedCount = demanders.stream().filter(Demander::getHasToBeIncluded).count();

        return maxGroupSizeSum >= demanderHasToBeIncludedCount;
    }

    public static boolean isInitializedValid(Collection<? extends Demander> demanders, Collection<? extends Offer> offers) {
        return isEveryOfferAbleToFulfillTheirMinSizeConstraint(demanders, offers)
                && isEveryDemanderThatHasToBeIncludedAbleToFindAGroup_light(demanders, offers);
    }

    public boolean isInitializedValid() {
        return isInitializedValid(members.stream().map(Member::getDemander).collect(Collectors.toSet()),
                groups.stream().map(Group::getOffer).collect(Collectors.toSet()));
    }

    // Requires information that is only available after the optimization (or too hard to check before)

    public boolean isEveryDemanderThatHasToBeIncludedInAGroup() {
        for (Member member : members) {
            if (member.insistsOnGroup() && !member.isInGroup()) {
                return false;
            }
        }
        return true;
    }

    // Getter und Setter ===============================================================================================

    public Collection<Group> getGroups() {
        return groups;
    }

    public Collection<Member> getMembers() {
        return members;
    }
}
