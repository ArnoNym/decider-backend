package an.decider.calc.output;

import an.decider.calc.input.Offer;

import java.util.Collection;

public interface Group {
    Offer getOffer();
    Collection<? extends Member> getMembers();

    default boolean isFull() {
        Integer maxGroupSize = getOffer().getMaxGroupSize();
        if (maxGroupSize == null) return false;
        return maxGroupSize == getMembers().size();
    }

    default boolean isEmpty() {
        return getMembers().isEmpty();
    }

    default double calcUtility() {
        double value = 0;
        for (Member member : getMembers()) {
            value += member.calcUtility();
        }
        return value;
    }
}
