package an.decider.calc.output;

import java.util.Collection;

public interface Choice {
    Collection<? extends Group> getGroups();

    default double calcUtility() {
        double utility = 0;
        for (Group group : getGroups()) {
            utility += group.calcUtility();
        }
        return utility;
    }
}
