package an.decider.calc.output;

import an.decider.Const;
import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;

public interface Member {
    Double getAddDemanderBonus();
    Demander getDemander();
    Group getGroup();

    default double calcUtility() {
        return getGroup() == null ? 0 : calcUtility(getGroup().getOffer());
    }
    default double calcUtility(Offer offer) {
        if (offer == null) {
            return 0;
        }
        double opinionValue = getDemanderOpinionValue(offer);
        if (Const.isNoWayUtility(opinionValue)) {
            return 0;
        }
        return opinionValue + getAddDemanderBonus();
    }
    default boolean isNoWay(Group group) {
        return isNoWay(group.getOffer());
    }
    default boolean isNoWay(Offer offer) {
        double opinionValue = getDemanderOpinionValue(offer);
        return Const.isNoWayUtility(opinionValue);
    }
    default boolean canJoin(Group group) {
        if (isNoWay(group)) return false;
        if (group.isFull()) return false;
        return true;
    }

    double getDemanderOpinionValue(Offer offer);
}
