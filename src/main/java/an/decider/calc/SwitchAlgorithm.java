package an.decider.calc;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class SwitchAlgorithm {
    public static boolean switchGroupsIfAddUtility(Member member, Collection<Member> members, Collection<Group> groups) {
        SwitchAlgorithm switchAlgorithm = new SwitchAlgorithm(member, members, groups);
        return switchAlgorithm.switchGroupsIfAddUtility();
    }

    private final Member member;
    private final Collection<Member> members;
    private final Collection<Group> groups;

    private SwitchAlgorithm(Member member, Collection<Member> members, Collection<Group> groups) {
        this.member = member;
        this.members = members;
        this.groups = groups;
    }

    public boolean switchGroupsIfAddUtility() {
        double maxAddUtility = 0;
        Member switchWithMember = null;
        Group switchWithMemberBestGroup = JoinAlgorithm.findBestGroup(member, groups);
        for (Member member : members) {
            double thisMemberAddUtility = this.member.calcJoinUtility(member.getGroup());
            if (thisMemberAddUtility > 0) {
                double otherMemberAddUtility = member.calcJoinUtility(switchWithMemberBestGroup);
                double addUtility = thisMemberAddUtility + otherMemberAddUtility;
                if (addUtility > maxAddUtility) {
                    maxAddUtility = addUtility;
                    switchWithMember = member;
                }
            }
        }
        if (maxAddUtility <= 0 ) {
            return false;
        }
        switchGroups(switchWithMember, switchWithMemberBestGroup);
        return true;
    }

    private void switchGroups(@NotNull Member other, @NotNull Group otherBestGroup) {
        if (member.isInGroup()) {
            member.joinGroup(null);
        }
        Group memberBestGroup = other.getGroup();
        if (!other.joinGroup(otherBestGroup)) throw new IllegalStateException();
        if (!member.joinGroup(memberBestGroup)) throw new IllegalStateException();
    }

    /**
     * calc join utility nicht nur für die eine sondern alle Gruppen damit jemand eine Gruppe verlaesst um Platz fuer
     * andere zu machen, auch wenn ihm die Gruppe besser gefaellt als dem anderen (wenn es den Gesamtnutzen erhoeh)
     */
}
