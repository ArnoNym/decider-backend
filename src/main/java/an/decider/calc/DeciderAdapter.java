package an.decider.calc;

import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Offer;
import an.decider.data.model.out.DataChoice;
import an.decider.data.model.out.DataGroup;
import an.decider.data.model.out.DataMember;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class DeciderAdapter {
    @NotNull
    public static List<DataChoice> calculateChoices(Decision decision) throws IllegalArgumentException {
        List<Choice> choices = Decider.calculateChoices(decision);
        List<DataChoice> dataChoices = new ArrayList<>(choices.size());
        for (Choice choice : choices) {
            dataChoices.add(pack(choice));
        }
        return dataChoices;
    }

    private static DataChoice pack(Choice choice) {
        Collection<Group> groups = choice.getGroups();
        Collection<Member> members = choice.getMembers();

        List<DataGroup> dataGroupList = new LinkedList<>();
        for (Group group : groups) {
            List<DataMember> dataMemberList = new LinkedList<>();
            DataGroup dataGroup = new DataGroup((Offer) group.getOffer(), dataMemberList);
            dataGroupList.add(dataGroup);

            for (Member member : group.getMembers()) {
                DataMember dataMember = new DataMember(member, dataGroup);
                //todo die Werte sind nicht korrekt. Vielleicht muss ich sich selbst oder Gruppenmitglieder entfernrn. Bzw die Gruppe in der er ist
                dataMemberList.add(dataMember);
            }
        }

        return new DataChoice(dataGroupList);
    }
}
