package an.decider.calc;

import an.decider.Const;
import an.decider.calc.input.Decision;
import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import an.decider.calc.methods.Choices;
import an.decider.calc.methods.Opinions;
import an.decider.data.validator.OfferValidator;
import an.decider.data.validator.ValidationException;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Decider {
    public static int maxChoicesOutputCount = 10;

    @NotNull
    public static List<Choice> calculateChoices(Decision decision) throws IllegalArgumentException {
        Collection<? extends Demander> demanders = decision.getDemanderList();
        Collection<? extends Offer> offers = decision.getOfferList();

        if (demanders.isEmpty() || offers.isEmpty()) {
            return new ArrayList<>();
        }

        if (!isValid(demanders, offers)) {
            throw new IllegalArgumentException();
        }

        List<Choice> choices = Choices.createChoices(decision.getAddDemanderBonus(), demanders, createSets(demanders, offers));

        optimize(choices);

        choices = extract(choices);

        return choices;
    }

    public static boolean isValid(@NotNull Collection<? extends Demander> demanders,
                                  @NotNull Collection<? extends Offer> offers) {
        //if (demanders.isEmpty()) return false;
        //if (offers.isEmpty()) return false;
        try {
            OfferValidator offerValidator = new OfferValidator();
            for (Offer offer : offers) {
                offerValidator.validate((an.decider.data.model.input.Offer) offer); // todo hoechst fragwuerdig das hier zu verwenden. Der Input sollte vorher gecheckt werden
            }
        } catch (ValidationException e) {
            return false;
        }
        return true;
    }

    public static List<List<Offer>> createSets(@NotNull Collection<? extends Demander> demanders,
                                               @NotNull Collection<? extends Offer> offers) {
        List<List<Offer>> sets = new ArrayList<>();

        // All offer set
        List<Offer> allSet = new ArrayList<>(offers.size());
        for (Offer offer : offers) {
            int available = offer.getAvailable(demanders.size());
            for (int i = 0; i < available; i++) {
                allSet.add(offer);
            }
        }
        sets.add(allSet);

        if (offers.size() == 1 && sets.get(0).size() == 1) {
            return sets;
        }

        // One offer sets
        for (Offer offer : offers) {
            List<Offer> set = new ArrayList<>(1);
            set.add(offer);
            sets.add(set);
        }

        return sets;
    }

    public static List<Choice> optimize(List<Choice> choices) {
        List<Choice> optimizedChoices = new ArrayList<>();
        for (Choice choice : choices) {
            if (choice.optimize()) {
                optimizedChoices.add(choice);
            }
        }
        return optimizedChoices;
    }

    public static List<Choice> extract(List<Choice> choices) {
        choices.sort((c1, c2) -> (int) ((c2.calcUtility() - c1.calcUtility()) * Const.UTILITY_ORDER_DECIMALS));
        List<Choice> extractedChoices = new ArrayList<>(maxChoicesOutputCount);
        for (int i = 0; i < Math.min(maxChoicesOutputCount, choices.size()); i++) {
            extractedChoices.add(choices.get(i));
        }
        return extractedChoices;
    }
}
