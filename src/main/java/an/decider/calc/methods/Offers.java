package an.decider.calc.methods;

import an.decider.calc.input.Offer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class Offers {
    @NotNull
    public static ArrayList<Offer> multiplyOffersByAvailability(int demanderCount, @NotNull Collection<? extends Offer> offers) {
        ArrayList<Offer> availableOfferArrayList = new ArrayList<>();
        for (Offer offer : offers) {
            int available = offer.getAvailableOptional()
                    .orElseGet(() -> (int) Math.ceil((double) demanderCount / (double) offer.getMinGroupSize()));
            for (int i = 0; i < available; i++) {
                availableOfferArrayList.add(offer);
            }
        }
        return availableOfferArrayList;
    }
}
