package an.decider.calc.methods;

import an.decider.calc.Member;
import an.decider.calc.input.Demander;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class Members {
    public static Set<Member> createMembers(double baseOpinionValue, Collection<? extends Demander> demanders) {
        Set<Member> members = new HashSet<>();
        for (Demander demander : demanders) {
            members.add(new Member(baseOpinionValue, demander));
        }
        return members;
    }

    public static boolean isHasToBeInGroupRequirementFulfilled(Collection<Member> members) {
        for (Member member : members) {
            if (member.insistsOnGroup() && !member.isInGroup()) {
                return false;
            }
        }
        return true;
    }

    @NotNull
    public static Set<Demander> getDemanders(@NotNull Collection<Member> members) {
        Set<Demander> demanders = new HashSet<>();
        for (Member member : members) {
            demanders.add(member.getDemander());
        }
        return demanders;
    }

    public static <T> boolean hasObjectWithThisRelationship(@NotNull Collection<T> ts,
                                                            boolean negative, boolean equal, boolean positive,
                                                            Function<T, Double> calcRelationshipFunction,
                                                            Predicate<T> fulfillsConstraintsPredicate) {
        for (T t : ts) {
            double relationship = calcRelationshipFunction.apply(t);
            if (relationship > 0) {
                if (positive) {
                    if (fulfillsConstraintsPredicate.test(t)) {
                        return true;
                    }
                }
            } else if (relationship == 0) {
                if (equal) {
                    if (fulfillsConstraintsPredicate.test(t)) {
                        return true;
                    }
                }
            } else if (relationship < 0) {
                if (negative) {
                    if (fulfillsConstraintsPredicate.test(t)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * So complicated because dataMember.equals cant be called, so collection.remove cant be called
     *
    @NotNull
    private static List<DataMember> removeDataMember(@NotNull DataMember dataMember, List<DataMember> dataMembers) {
        List<DataMember> otherDataMembers = new LinkedList<>(dataMembers);
        Member member = dataMember.getMember().orElseThrow();
        Iterator<DataMember> dataMemberIterator = otherDataMembers.iterator();
        while (dataMemberIterator.hasNext()) {
            DataMember otherDataMember = dataMemberIterator.next();
            if (member.equals(otherDataMember.getMember().orElseThrow())) {
                dataMemberIterator.remove();
                break;
            }
        }
        return otherDataMembers;
    }
    */
}
