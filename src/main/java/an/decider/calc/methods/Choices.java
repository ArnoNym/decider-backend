package an.decider.calc.methods;

import an.decider.calc.Choice;
import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import an.decider.utils.SetUtils;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Choices {
    public static List<Choice> createChoices(double baseOpinionValue,
                                             @NotNull Collection<? extends Demander> demanders,
                                             @NotNull Collection<? extends Collection<Offer>> offerPowerSet) {
        List<Choice> choices = new ArrayList<>(offerPowerSet.size());
        for (Collection<Offer> offers : offerPowerSet) {
            Choice choice = new Choice(baseOpinionValue, demanders, offers);
            choices.add(choice);
        }
        return choices;
    }

    /**
     * Remove Choices where Offers that have to be included are missing
     *
    public static void removeInvalidSets(@NotNull Collection<? extends Demander> demanders,
                                          @NotNull Collection<? extends Offer> allOffers,
                                          @NotNull Set<List<Offer>> offerPowerSet) {
        for (List<Offer> offerSet : offerPowerSet) {
            for (Offer offer : offerSet) {
                assert allOffers.contains(offer);
            }
        }

        int hasToBeIncludedCount = 0;
        for (Offer offer : allOffers) {
            if (offer.getHasToBeIncluded()) {
                hasToBeIncludedCount += offer.getAvailableOptional()
                        .orElseThrow(() -> new IllegalArgumentException("Allow infinitely many and force all of them to be included isn't allowed!"));
            }
        }

        Iterator<List<Offer>> offerListIterator = offerPowerSet.iterator();
        while (offerListIterator.hasNext()) {
            List<Offer> offerSet = offerListIterator.next();
            if (!Choice.isEveryRequiredOfferIncluded(hasToBeIncludedCount, offerSet) || !Choice.isInitializedValid(demanders, offerSet)) {
                offerListIterator.remove();
            }
        }
    }*/
}
