package an.decider.calc.methods;

import an.decider.calc.Group;
import an.decider.calc.input.Offer;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Groups {
    public static Set<Group> createGroups(Collection<? extends Offer> offers) {
        Set<Group> groups = new HashSet<>();
        for (Offer offer : offers) {
            groups.add(new Group(offer));
        }
        return groups;
    }

    @NotNull
    public static Set<Offer> getOffers(@NotNull Collection<Group> groups) {
        Set<Offer> offers = new HashSet<>();
        for (Group group : groups) {
            offers.add(group.getOffer());
        }
        return offers;
    }

    /**
     * So complicated because dataGroup.equals cant be called, so collection.remove cant be called
     *
    @NotNull
    private static List<DataGroup> removeDataGroup(@NotNull DataGroup dataGroup, List<DataGroup> dataGroups) {
        List<DataGroup> otherDataGroups = new LinkedList<>(dataGroups);
        Group group = dataGroup.getGroup().orElseThrow();
        Iterator<DataGroup> dataGroupIterator = otherDataGroups.iterator();
        while (dataGroupIterator.hasNext()) {
            DataGroup otherDataGroup = dataGroupIterator.next();
            if (group.equals(otherDataGroup.getGroup().orElseThrow())) {
                dataGroupIterator.remove();
                break;
            }
        }
        return otherDataGroups;
    }
    */
}
