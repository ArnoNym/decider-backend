package an.decider.calc;

import java.util.*;

import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class Group implements an.decider.calc.output.Group {
    @NotNull
    private final Offer offer;
    @NotNull
    private final Set<Member> members = new HashSet<>();

    public Group(@NotNull Offer offer) {
        this.offer = offer;
    }

    @Contract(pure = true)
    public double calcUtility() {
        double utility = 0;
        for (Member member : members) {
            utility += member.calcUtility(this);
        }
        return utility;
    }

    // members ========================================================================================================

    @Contract(pure = true, value = " -> new")
    public Set<Member> getMembers() {
        return new HashSet<>(members);
    }

    @Contract(pure = true, value = " -> new")
    public Set<Demander> getDemanders() {
        Set<Demander> currentDemanders = new HashSet<>();
        for (an.decider.calc.Member member : members) {
            currentDemanders.add(member.getDemander());
        }
        return currentDemanders;
    }

    public boolean add(Member member) {
        if (isAddAllowed()) {
            return members.add(member);
        }
        return false;
    }

    public boolean isAddAllowed(Member member) {
        return isAddAllowed() && !members.contains(member);
    }

    private boolean isAddAllowed() {
        Optional<Integer> maxGroupSize = offer.getMaxGroupSizeOptional();
        return maxGroupSize.isEmpty() || maxGroupSize.get() > size();
    }

    public boolean remove(Member member) {
        return members.remove(member);
    }

    public boolean contains(Member member) {
        return members.contains(member);
    }

    public boolean isEmpty() {
        return members.isEmpty();
    }

    public int size() {
        return members.size();
    }

    // ================================================================================================================

    @NotNull
    public Offer getOffer() {
        return offer;
    }

    public int getMinGroupSize() {
        return offer.getMinGroupSize();
    }

    public int getMaxGroupSize() {
        return offer.getMaxGroupSizeOptional().orElse(Integer.MAX_VALUE);
    }

    public boolean fulfillsMinGroupSizeConstraint() {
        return getMinGroupSize() <= size();
    }

    // ================================================================================================================

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Offer: ").append(offer.toString()).append(", Demanders: ");
        for (an.decider.calc.Member member : members) {
            stringBuilder.append(member.toString()).append(", ");
        }
        return stringBuilder.toString();
    }
}
