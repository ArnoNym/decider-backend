package an.decider.calc.input;

import java.util.Optional;

public interface Offer {
    String getId();

    Boolean getHasToBeIncluded();

    Integer getAvailable();
    default Optional<Integer> getAvailableOptional() {
        return Optional.ofNullable(getAvailable());
    }
    default int getAvailable(int demanderCount) {
        return getAvailableOptional().orElse((int) Math.ceil((float) demanderCount / Math.max(1, getMinGroupSize())));
    }

    Integer getMinGroupSize();

    Integer getMaxGroupSize();
    default Optional<Integer> getMaxGroupSizeOptional() {
        return Optional.ofNullable(getMaxGroupSize());
    }
}
