package an.decider.calc.input;

import java.util.List;

public interface Decision {
    Integer getMaxOpinionValue();
    Double getAddDemanderBonus();
    List<? extends Offer> getOfferList();
    List<? extends Demander> getDemanderList();
}
