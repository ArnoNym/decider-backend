package an.decider.calc.input;

import java.util.Collection;

public interface Demander {
    Boolean getHasToBeIncluded();
    Collection<? extends Opinion> getOpinions();
}
