package an.decider.calc.input;

import an.decider.Const;

public interface Opinion {
    Offer getOffer();
    Demander getDemander();
    Double getValue();
    default boolean isNoWayValue() {
        return Const.isNoWayUtility(getValue());
    }
}
