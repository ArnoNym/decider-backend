package an.decider.calc;

import java.util.*;
import java.util.function.Function;

import an.decider.Const;
import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import an.decider.calc.input.Opinion;
import an.decider.calc.methods.Opinions;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Member implements an.decider.calc.output.Member {
    private final Demander demander;
    private Group group;
    private final double baseOpinionValue;

    public Member(double baseOpinionValue, @NotNull Demander demander) {
        this.baseOpinionValue = baseOpinionValue;
        this.demander = demander;
    }

    // join Groups ================================================================================

    public boolean joinBestGroup(@NotNull Collection<Group> groups) {
        return JoinAlgorithm.joinBestGroup(this, groups);
    }

    boolean joinGroup(@Nullable Group group) {
        if (group != null) {
            if (isNoWay(group)) {
                return false;
            }
            if (!group.add(this)) {
                return false;
            } else {
                if (Const.DEVELOPER_OUT_MAX) System.out.println("JoinedGroup: memberHashCode="+hashCode()+", groupHashCode="+group.hashCode()+", size="+group.size()+", minSize="
                        +group.getMinGroupSize()+", maxSize="+group.getMaxGroupSize()+", groupUtility="+ group.calcUtility());
            }
        }
        if (this.group != null) {
            if (Const.DEVELOPER_OUT_MAX) System.out.println("OldGroup: size="+this.group.size()+", minSize="
                    +this.group.getMinGroupSize()+", maxSize="+this.group.getMaxGroupSize()+", utility="+this.calcUtility(group));
            this.group.remove(this);
        }
        this.group = group;
        return true;
    }

    // Switch Groups ==================================================================================================

    public boolean switchGroupsIfAddUtility(@NotNull Collection<Member> members, @NotNull Collection<Group> groups) {
        return SwitchAlgorithm.switchGroupsIfAddUtility(this, members, groups);
        /*
        double maxAddUtility = 0;
        Member switchWithMember = null;
        for (Member member : members) {
            double addUtility = calcSwitchUtility(member);
            if (addUtility > maxAddUtility) {
                maxAddUtility = addUtility;
                switchWithMember = member;
            }
        }
        if (maxAddUtility <= 0) {
            return false;
        }
        switchGroups(switchWithMember);
        return true;
        */
    }

    private void switchGroups(@NotNull Member other) {
        if (this.group != null) {
            if (!this.group.remove(this)) throw new IllegalStateException();
            if (!this.group.add(other)) throw new IllegalStateException();
        }
        if (other.group != null) {
            if (!other.group.remove(other)) throw new IllegalStateException();
            if (!other.group.add(this)) throw new IllegalStateException();
        }
        Group thisOldGroup = this.group;
        this.group = other.group;
        other.group = thisOldGroup;
    }

    // utility ========================================================================================================

    public double calcUtility(Group group) {
        if (group == null) {
            return 0;
        }
        return calcUtility(group.getOffer());
    }

    double calcJoinUtility(@Nullable Group group) {
        return calcUtility(group) - calcUtility();
    }

    private double calcSwitchUtility(@NotNull Member other) {/*todo calc join utility nicht nur für die eine sondern alle Gruppen
            damit jemand eine Gruppe verlaesst um Platz fuer andere zu machen, auch wenn ihm die Gruppe besser gefaellt
        als dem anderen (wenn es den Gesamtnutzen erhoeh)*/
        return this.calcJoinUtility(other.getGroup()) + other.calcJoinUtility(this.getGroup());
    }

    public boolean isEqualUtilityGroup(Group group) {
        if (this.group == null) {
            throw new IllegalArgumentException();
        }
        if (group == null) {
            throw new IllegalArgumentException();
        }
        return calcJoinUtility(group) == 0;
    }

    // opinion =========================================================================================================

    private Map<Offer, Opinion> offerOpinionMap;

    @Override
    public double getDemanderOpinionValue(Offer offer) {
        if (offerOpinionMap == null) {
            createMap();
        }
        Opinion opinion = offerOpinionMap.get(offer);
        if (opinion == null) {
            createMap();
            opinion = offerOpinionMap.get(offer);
        }
        return opinion.getValue();
    }

    private void createMap() {
        offerOpinionMap = new HashMap<>();
        for (Opinion opinion : demander.getOpinions()) {
            offerOpinionMap.put(opinion.getOffer(), opinion);
        }
    }

    // ================================================================================================================

    @Contract(pure = true)
    public boolean insistsOnGroup() {
        return demander.getHasToBeIncluded();
    }

    public Optional<Group> getGroupOptional() {
        return Optional.ofNullable(group);
    }

    @Override
    public Group getGroup() {
        return group;
    }

    public boolean isInGroup() {
        return group != null;
    }

    public Demander getDemander() {
        return demander;
    }

    public boolean equalGroup(Group group) {
        if (this.group == null) {
            throw new IllegalArgumentException();
        }
        if (group == null) {
            throw new IllegalArgumentException();
        }
        return this.group.equals(group);
    }

    @Override
    public Double getAddDemanderBonus() {
        return baseOpinionValue;
    }
}
