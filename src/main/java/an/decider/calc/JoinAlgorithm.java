package an.decider.calc;

import an.decider.Const;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class JoinAlgorithm {
    public static boolean joinBestGroup(Member member, @NotNull Collection<Group> groups) {
        JoinAlgorithm ja = new JoinAlgorithm(member, groups);
        return ja.joinBestGroup();
    }
    public static Group findBestGroup(Member member, @NotNull Collection<Group> groups) {
        JoinAlgorithm ja = new JoinAlgorithm(member, groups);
        return ja.findBestGroup();
    }

    private final Member member;
    private Collection<Group> groups;

    private JoinAlgorithm(Member member, Collection<Group> groups) {
        this.member = member;
        this.groups = groups.stream().filter(member::canJoin).collect(Collectors.toSet());
        if (member.isInGroup() && !member.canJoin(member.getGroup())) this.groups.add(member.getGroup());
    }

    // join Groups ================================================================================

    /**
     * define best group (if 1 leaves more than one option let 2 decide and so on)
     * 1. group maximizes utility for this member
     * 2. group still needs members for min member constraints
     * 3. group has less members than equal groups
     * 4. random
     * @return true if member joined another group
     */
    public boolean joinBestGroup() {
        Group bestGroup = findBestGroup();

        if (bestGroup == null) {
            return false;
        }

        if (bestGroup.equals(member.getGroup())) {
            return false;
        }

        if (member.joinGroup(bestGroup)) {
            return true;
        } else {
            throw new IllegalStateException("Das sollte doch eigentlich micht mehr vorkommen koennen, odeR??");
        }
    }

    public Group findBestGroup() {
        if (groups.size() == 1) System.out.println("Join only Option!");
        if (groups.size() > 1) {
            groups = findBestGroups_byUtility(groups);
            if (Const.DEVELOPER && groups.size() == 1) System.out.println("Join by Utility!");
        }
        if (groups.size() > 1) {
            groups = findBestGroups_byMinDiffToMinSizeConstraint(groups);
            if (Const.DEVELOPER && groups.size() == 1) System.out.println("Join by MinDiffToMinSize!");
        }
        if (groups.size() > 1) {
            groups = findBestGroups_byMaxSize(groups);
            if (Const.DEVELOPER && groups.size() == 1) System.out.println("Join by MinSize!");
        }
        if (groups.contains(member.getGroup())) {
            return member.getGroup();
        }
        if (Const.DEVELOPER && groups.size() > 1) System.out.println("Join random!");
        return groups.isEmpty() ? null : groups.iterator().next();
    }

    private Collection<Group> findBestGroups_byUtility(@NotNull Collection<Group> options) {
        return findBestGroups(options, member::calcUtility, 0);
    }

    /**
     * Gruppen nach moeglichkeit zu stande kommen lassen
     */
    private Collection<Group> findBestGroups_byMinDiffToMinSizeConstraint(@NotNull Collection<Group> options) {
        Function<Group, Double> rateGroupFunction = group -> {
            double rating;
            if (group.isEmpty() || group.fulfillsMinGroupSizeConstraint()) {
                rating = Double.NEGATIVE_INFINITY;
            } else {
                rating = group.size() - group.getMinGroupSize();
                if (group.equals(member.getGroup())) {
                    rating -= 1;
                }
            }
            return rating;
        };
        return findBestGroups(options, rateGroupFunction, Double.NEGATIVE_INFINITY);
    }

    private Collection<Group> findBestGroups_byMaxSize(@NotNull Collection<Group> options) {
        Function<Group, Double> rateGroupFunction = group -> {
            double rating = group.size();
            if (group.equals(member.getGroup())) {
                rating -= 1;
            }
            return rating;
        };
        return findBestGroups(options, rateGroupFunction, Double.NEGATIVE_INFINITY);
    }

    public Collection<Group> findBestGroups(@NotNull Collection<Group> options,
                                            @NotNull Function<Group, Double> ratingFunction,
                                            final double requiredMinRating) {
        if (options.isEmpty()) {
            return options;
        }

        List<Group> selectedGroups = new LinkedList<>();
        double maxRating = requiredMinRating;
        for (Group group : options) {
            double rating = Objects.requireNonNull(ratingFunction.apply(group));
            if (rating >= maxRating) {
                if (rating > maxRating) {
                    maxRating = rating;
                    selectedGroups.clear();
                }
                selectedGroups.add(group);
            }
        }

        if (selectedGroups.isEmpty()) {
            options.clear();
            return options;
        } else if (selectedGroups.size() > 1) {
            options.clear();
            options.addAll(selectedGroups);
            return options;
        } else {
            Group maxRatingGroup = selectedGroups.get(0);

            if (!member.isInGroup() || maxRating > requiredMinRating) {
                options.clear();
                options.add(maxRatingGroup);
                return options;
            }

            assert maxRating == requiredMinRating;

            options.clear();
            options.add(maxRatingGroup);
            return options;
        }
    }

}
