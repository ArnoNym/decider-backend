package an.decider.services;

import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.MemberDao;
import an.decider.data.model.input.Demander;
import an.decider.data.model.out.DataGroup;
import an.decider.data.model.out.DataMember;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
@RequiredArgsConstructor
public class MemberService {
    private final MemberDao memberDao;
    private final DemanderDao demanderDao;

    public void updateGroupMembership(Collection<DataMember> dataMembers) {
        memberDao.saveAllMinimal(dataMembers);
    }

    public void fillDemanderByDemanderId(String decisionId, Collection<DataMember> toMembers) {
        if (decisionId == null) {
            throw new IllegalArgumentException();
        }
        List<Demander> fromDemanders = demanderDao.findAllByDecisionId(decisionId);
        if (toMembers.size() > fromDemanders.size()) {
            throw new IllegalArgumentException();
        }
        for (DataMember toMember : toMembers) {
            String toDemanderId = toMember.getDemander().getId();
            if (toDemanderId == null) {
                throw new IllegalArgumentException();
            }
            for (Demander fromDemander : fromDemanders) {
                if (fromDemander.getId().equals(toDemanderId)) {
                    toMember.setDemander(fromDemander);
                }
            }
        }
    }
}
