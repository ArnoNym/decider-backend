package an.decider.services;

import an.decider.data.api.DecisionApi;
import an.decider.data.dao.DecisionDao;
import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.OfferDao;
import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import an.decider.data.model.input.Opinion;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OfferService {
    private final OfferDao offerDao;
    private final DemanderDao demanderDao;
    private final DecisionApi decisionApi;
    private final DecisionDao decisionDao;

    public Offer createInviteOffer(String inviteId) {
        String decisionId = decisionDao.findIdByInviteId(inviteId);
        Decision decision = decisionDao.findById(decisionId).orElseThrow(); // todo optimieren?!
        decision.setId(null);

        Offer offer = new Offer();
        offer.setDecision(decision);

        return offer;
    }

    public void saveInviteOffer(@NotNull Offer offer) {
        String decisionId = decisionApi.findIdByInviteId(offer.getDecision().getInviteId());
        offer.getDecision().setId(decisionId);

        offer.setId(Utils.generateUUID());

        List<Opinion> opinionList = new LinkedList<>();
        for (Demander demander : demanderDao.findAllByDecisionId(decisionId)) {
            Opinion opinion = new Opinion();
            opinion.setDemander(demander);
            opinion.setOffer(offer);
            opinionList.add(opinion);
        }
        for (Opinion opinion : opinionList) {
            opinion.setId(Utils.generateUUID());
        }
        offer.setOpinionList(opinionList);

        offerDao.save(offer);
    }

    public List<Offer> findAllByDecisionId(String decisionId) {
        return offerDao.findAllByDecisionId(decisionId);
    }
}
