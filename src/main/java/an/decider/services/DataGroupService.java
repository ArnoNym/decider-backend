package an.decider.services;

import an.decider.calc.output.Group;
import an.decider.calc.output.Member;
import an.decider.data.dao.DataGroupDao;
import an.decider.data.model.out.DataGroup;
import an.decider.data.model.out.DataMember;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class DataGroupService {
    private final DataGroupDao dataGroupDao;
    private final MemberService memberService;

    public static <M extends Member> List<M> getAllMembers(Collection<? extends Group> groups) {
        List<M> allDataMemberList = new LinkedList<>();
        for (Group group : groups) {
            Collection<M> members = (Collection<M>) group.getMembers();
            if (members != null) { // null if contains no dataMembers
                allDataMemberList.addAll(members);
            }
        }
        return allDataMemberList;
    }

    /*
    public void addOffers(Collection<? extends DataGroup> dataGroups) {
        if (dataGroups.isEmpty()) {
            return;
        }

        Set<String> dataGroupIds = new HashSet<>();
        for (DataGroup dataGroup : dataGroups) {
            String id = dataGroup.getId();
            if (Const.DEVELOPER && id == null) {
                String error = "\n";
                error += "All DataGroups must have an ID.\n";
                throw new IllegalArgumentException(error);
            }
            dataGroupIds.add(id);
        }

        Collection<DataGroup> dataGroupsWithOffers = dataGroupDao.findAllOfferMinAndMaxGroupSizeByDataGroupIds(dataGroupIds);
        for (DataGroup dataGroup : dataGroups) {
            List<DataGroup> dataGroupsWithOffer = new ArrayList<>(dataGroupsWithOffers);
            dataGroupsWithOffer.removeIf(dg -> !dg.getId().equals(dataGroup.getId()));
            DataGroup dataGroupWithOffer = dataGroupsWithOffer.get(0);
            dataGroup.setOffer(dataGroupWithOffer.getOffer());
        }
    }
    */

    public void checkGroup(Collection<? extends DataGroup> dataGroups, boolean minGroupSizeConstraint,
                           boolean maxGroupSizeConstraint) throws IllegalArgumentException {
        if (!minGroupSizeConstraint && !maxGroupSizeConstraint) {
            throw new IllegalArgumentException();
        }

        for (DataGroup dataGroup : dataGroups) {
            if (minGroupSizeConstraint && !dataGroup.fulFillsMinGroupSizeConstraint()) {
                throw new IllegalArgumentException("DataGroup with ID=" + dataGroup.getId() + " has to contain at least "
                        + dataGroup.getOffer().getMinGroupSize()
                        + " members. Current = " + dataGroup.getDataMemberListSize());
            }
            if (maxGroupSizeConstraint && !dataGroup.fullFillsMaxGroupSizeConstraint()) {
                throw new IllegalArgumentException("DataGroup with ID=" + dataGroup.getId() + " may not contain more than "
                        + dataGroup.getOffer().getMaxGroupSizeOptional().orElse(Integer.MAX_VALUE)
                        + " members. Current = " + dataGroup.getDataMemberListSize());
            }
        }
    }

    public void overrideAllGroups(List<DataGroup> dataGroupList) throws IllegalArgumentException {
        if (dataGroupList.isEmpty()) return;
        String choiceId = dataGroupList.get(0).getDataChoice().getId();
        dataGroupDao.removeAllByDataChoiceId(choiceId);
        dataGroupDao.saveAll(dataGroupList);
    }

    public void saveNewGroups(Iterable<DataGroup> oldGroups, Collection<DataGroup> currentGroups) {
        Set<DataGroup> newGroups = new HashSet<>(currentGroups);
        for (DataGroup oldGroup : oldGroups) {
            newGroups.remove(oldGroup);
        }
        dataGroupDao.saveAll(newGroups);
    }

    public void saveNewGroups(Collection<DataGroup> newGroups) {
        checkGroup(newGroups, false, true);
        for (DataGroup dataGroup : newGroups) {
            dataGroup.setId(Utils.generateUUID());
        }
        dataGroupDao.saveAll(newGroups);
    }

    public void updateGroupMembership(List<DataGroup> dataGroupList) throws IllegalArgumentException {
        /* When an object ist added to index n, empty Objects are created for index 0 to n-1. These Objects need to be
        removed in order to get the collections actual size */
        for (DataGroup dataGroup : dataGroupList) {
            dataGroup.getDataMemberList().removeIf(dataMember -> Objects.isNull(dataMember.getId()));
        }

        checkGroup(dataGroupList, false, true);

        List<DataMember> allDataMemberList = new LinkedList<>();
        for (DataGroup dataGroup : dataGroupList) {
            List<DataMember> dataMemberList = dataGroup.getDataMemberList();

            if (dataMemberList != null) { // null if contains no dataMembers
                for (DataMember dataMember : dataMemberList) {
                    dataMember.setDataGroup(dataGroup);
                }
                allDataMemberList.addAll(dataMemberList);
            }
        }
        memberService.updateGroupMembership(allDataMemberList);
    }

    /**
     * Takes offer and demander ids
     * - to fill in this decisions matching member and group ids
     * - to fill in missing fields of offers and demanders
     */
    public void fillOuterObjectIdAndInnerObjectByInnerObjectId(List<DataGroup> from, List<DataGroup> to) {
        Utils.fillObjectByMatchingIds(from, to,
                group -> group.getOffer().getId(),
                (fromGroup, toGroup) -> toGroup.setOffer(fromGroup.getOffer()));
        Collection<DataMember> memberFrom = getAllMembers(from);
        for (DataGroup dataGroup : to) {
            Utils.fillObjectByMatchingIds(memberFrom, dataGroup.getDataMemberList(),
                    member -> member.getDemander().getId(),
                    (fromMember, toMember) -> toMember.setDemander(fromMember.getDemander()));
        }
    }
}
