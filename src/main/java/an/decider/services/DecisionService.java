package an.decider.services;

import an.decider.data.api.ChoiceApi;
import an.decider.data.api.DecisionApi;
import an.decider.data.api.DemanderApi;
import an.decider.data.api.OfferApi;
import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import an.decider.data.model.input.Opinion;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class DecisionService {
    private final TransactionWorker transactionWorker;

    private final DecisionApi decisionApi;
    private final ChoiceApi choiceApi;

    private final DemanderApi demanderApi;
    private final OfferApi offerApi;

    public Decision getDecision(String id) throws NoSuchElementException {
        Decision decision = decisionApi.findById(id);

        sort(decision);

        return decision;
    }
    
    public void sort(Decision decision) {
        decision.getDemanderList().sort(Comparator.comparing(Demander::getName));
        decision.getOfferList().sort(Comparator.comparing(Offer::getName));
        for (Demander demander : decision.getDemanderList()) {
            demander.getOpinionList().sort(Comparator.comparing(opinion -> opinion.getOffer().getName()));
        }
    }

    public String getDecisionNameByChoiceId(String choiceId) throws NoSuchElementException {
        return decisionApi.findNameByChoiceId(choiceId);
    }

    public void updateSettings(Decision decision) throws IllegalArgumentException {
        decisionApi.updateSettings(decision);
    }

    public void updateInvite(@NotNull String decisionId, Boolean demanderLinkActive, Boolean offerLinkActive) {
        if (demanderLinkActive == null && offerLinkActive == null) {
            throw new IllegalArgumentException();
        }
        /* Only one can be active at the same time. Which means even if one link gets deactivated we know that the
        other had to be deactivated before. */
        if (demanderLinkActive != null) {
            offerLinkActive = false;
        } else {
            demanderLinkActive = false;
        }
        decisionApi.updateInvite(decisionId, demanderLinkActive, offerLinkActive);
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void saveOverview(@Valid Decision decision) throws ValidationException {
        String decisionId = decision.getId();
        if (decisionId == null) throw new IllegalArgumentException("DecisionId may not be null!");

        setIds(decision);
        cleanUp(decision);

        assert decisionId.equals(decision.getId());

        if (decisionApi.isCurrentVersion(decisionId, decision.getVersion())) {
            decision.setVersion(decision.getVersion() + 1);
        } else {
            mergeWithDatabaseVersion(decision);
        }

        //System.out.println(decision.toString());

        transactionWorker.set(() -> offerApi.saveAll(decision.getOfferList()));
        transactionWorker.set(() -> demanderApi.saveAll(decision.getDemanderList()));

        Set<String> offerIds = new HashSet<>();
        for (Offer offer : decision.getOfferList()) {
            offerIds.add(offer.getId());
        }
        List<Offer> deletedOffers = transactionWorker.get(() -> offerApi.deleteAllByIdNotIn(decisionId, offerIds));
        Set<String> demanderIds = new HashSet<>();
        for (Demander demander : decision.getDemanderList()) {
            demanderIds.add(demander.getId());
        }
        System.out.println("Dont delete this many demanders by id: "+demanderIds.size());
        List<Demander> deletedDemanders = transactionWorker.get(() -> demanderApi.deleteAllByIdNotIn(decisionId, demanderIds));
        System.out.println("Deleted demanders: "+deletedDemanders.size());

        choiceApi.deleteAllByDecisionId(decision.getId());
    }

    private void mergeWithDatabaseVersion(Decision decision) {
        Set<Demander> demanders = new HashSet<>(decision.getDemanderList());
        Set<Offer> offers = new HashSet<>(decision.getOfferList());

        demanders.addAll(transactionWorker.get(() -> demanderApi.findAll(decision.getId())));
        offers.addAll(transactionWorker.get(() -> offerApi.findAll(decision.getId())));

        decision.setDemanderList(new ArrayList<>(demanders));
        decision.setOfferList(new ArrayList<>(offers));
    }

    private void setIds(Decision decision) {
        List<Offer> offerList = decision.getOfferList();
        List<Demander> demanderList = decision.getDemanderList();

        for (Offer offer : offerList) {
            offer.setDecision(decision);
            if (offer.requiresId()) {
                offer.setId(Utils.generateUUID());
            }
        }

        for (Demander demander : demanderList) {
            demander.setDecision(decision);
            if (demander.requiresId()) {
                demander.setId(Utils.generateUUID());
            }
            for (Opinion opinion : demander.getOpinionList()) {
                if (opinion.requiresId()) {
                    opinion.setDemander(demander);
                    opinion.setOffer(offerList.get(opinion.getOffer().getIndex()));
                    opinion.setId(Utils.generateUUID());
                }
            }
        }
    }

    /**
     * Wenn Objekte geloescht werden bleiben Felde in den Listen frei. Hybernate erstellt an diesen Stellen leere
     * Objekte. Diese werden hier geloescht todo bessere loesung?
     */
    private void cleanUp(Decision decision) {
        decision.getOfferList().removeIf(Offer::shouldGetDeleted);
        decision.getDemanderList().removeIf(Demander::shouldGetDeleted);
        for (Demander demander : decision.getDemanderList()) {
            demander.getOpinionList().removeIf(Opinion::shouldGetDeleted);
        }
    }

    public String saveNew(String name) {
        Decision decision = new Decision();
        decision.setName(name);
        decisionApi.save(decision);
        return decision.getId();
    }

    public String getDecisionName(String decisionId) {
        return decisionApi.findDecisionNameByDecisionId(decisionId);
    }
}
