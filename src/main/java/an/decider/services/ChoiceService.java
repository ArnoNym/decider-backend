package an.decider.services;

import an.decider.calc.DeciderAdapter;
import an.decider.data.api.ChoiceApi;
import an.decider.data.api.DecisionApi;
import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.OfferDao;
import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import an.decider.data.model.out.DataChoice;
import an.decider.data.model.out.DataGroup;
import an.decider.data.model.out.DataMember;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ChoiceService {
    private final ChoiceApi choiceApi;
    private final DataGroupService dataGroupService;
    private final MemberService memberService;
    private final DecisionApi decisionApi;
    private final OfferDao offerDao;
    private final DemanderDao demanderDao;

    public List<DataChoice> getAll(String decisionId) throws NoSuchElementException {
        List<DataChoice> dataChoiceList = choiceApi.findAllByDecisionId(decisionId);
        if (dataChoiceList.isEmpty()) {
            Decision decision = decisionApi.findById(decisionId);
            dataChoiceList = calculateChoices(decision);
            choiceApi.saveAll(decision.getId(), dataChoiceList);
        } else {
            dataChoiceList.sort((o1, o2) -> (int) ((o2.calcUtility() - o1.calcUtility()) * 10000));
        }
        return dataChoiceList;
    }

    private List<DataChoice> calculateChoices(Decision decision) {
        List<DataChoice> dataChoiceList = DeciderAdapter.calculateChoices(decision);
        dataChoiceList = removeEmptyGroups(dataChoiceList);
        setGeneratedIds(decision, dataChoiceList);
        return dataChoiceList;
    }

    private List<DataChoice> removeEmptyGroups(List<DataChoice> dataChoiceList) {
        for (DataChoice dataChoice : dataChoiceList) {
            dataChoice.setDataGroupList(dataChoice.getDataGroupList().stream()
                    .filter(dataGroup -> !dataGroup.isEmpty())
                    .collect(Collectors.toList()));
        }
        dataChoiceList = dataChoiceList.stream()
                .filter(dataChoice -> !dataChoice.getDataGroupList().isEmpty())
                .collect(Collectors.toList());
        return dataChoiceList;
    }

    public DataChoice findModifyChoice(String dataChoiceId) throws NoSuchElementException {
        DataChoice dataChoice = choiceApi.findById(dataChoiceId);

        setUnusedDemanders(dataChoice);
        createMissingOfferGroups(dataChoice);

        sort(dataChoice);

        return dataChoice;
    }
    public DataChoice createModifyChoice(String decisionId) {
        DataChoice dataChoice = new DataChoice();
        dataChoice.setDecision(new Decision());
        dataChoice.getDecision().setId(decisionId);

        setUnusedDemanders(dataChoice);
        createMissingOfferGroups(dataChoice);

        sort(dataChoice);

        return dataChoice;
    }
    private void createMissingOfferGroups(DataChoice dataChoice) {
        List<Offer> offerList = offerDao.findAllByDecisionId(dataChoice.getDecision().getId());

        Map<Offer, Integer> offerCountMap = new HashMap<>();

        if (dataChoice.getDataGroupList() != null) {
            for (DataGroup dataGroup : dataChoice.getDataGroupList()) {
                Offer offer = dataGroup.getOffer();
                Integer offerCount = offerCountMap.get(offer);
                offerCountMap.put(offer, offerCount == null ? 1 : offerCount + 1);
            }
        } else {
            dataChoice.setDataGroupList(new ArrayList<>());
        }

        int demanderCount = dataChoice.calcDemanderCount();
        for (Offer offer : offerList) {
            Integer offerCount = offerCountMap.get(offer);
            for (int i = offerCount == null ? 0 : offerCount; i < offer.getAvailable(demanderCount); i++) {
                dataChoice.getDataGroupList().add(new DataGroup(dataChoice, offer));
            }
        }
    }
    private void setUnusedDemanders(DataChoice dataChoice) {
        if (dataChoice.getDecision().getId() == null) {
            throw new IllegalArgumentException();
        }

        Collection<Demander> demanders;

        Collection<DataMember> allMembers = dataChoice.calcMemberList();

        for (DataMember dataMember : allMembers) {
            System.out.println(dataMember.toString());
        }

        if (allMembers.isEmpty()) {
            demanders = demanderDao.findAllByDecisionId(dataChoice.getDecision().getId());
        } else {
            Set<String> presentDemanderIds = allMembers.stream()
                    .map(member -> member.getDemander().getId())
                    .collect(Collectors.toSet());

            for (String id : presentDemanderIds) {
                System.out.println("id: "+id);
            }

            demanders = demanderDao.findByDecisionIdAndIdNotIn(dataChoice.getDecision().getId(), presentDemanderIds);
        }

        for (Demander demander : demanders) {
            System.out.println(demander.toString());
        }

        dataChoice.setUnusedDemanders(new ArrayList<>(demanders));
    }

    public void sort(DataChoice dataChoice) {
        dataChoice.getDataGroupList().sort(Comparator.comparing(dataGroup -> dataGroup.getOffer().getName()));
        for (DataGroup dataGroup : dataChoice.getDataGroupList()) {
            dataGroup.getDataMemberList().sort(Comparator.comparing(dataMember -> dataMember.getDemander().getName()));
        }
    }

    public DataChoice find(String dataChoiceId) throws NoSuchElementException {
        return choiceApi.findById(dataChoiceId);
    }

    public void save(DataChoice dataChoice, boolean override) {
        removeEmptyObjects(dataChoice);

        validateInput(dataChoice, override);

        if (override) {
            setGeneratedIds(dataChoice);
        } else {
            setGeneratedIds(dataChoice.getDecision(), dataChoice);
        }

        List<DataGroup> groupList = dataChoice.getDataGroupList();
        List<DataMember> memberList = DataGroupService.getAllMembers(dataChoice.getDataGroupList());

        memberService.fillDemanderByDemanderId(dataChoice.getDecision().getId(), memberList);

        dataChoice.setModified(true);

        choiceApi.save(dataChoice);
    }

    public void validateInput(DataChoice choice, boolean override) {
        if (override && !choiceApi.findById(choice.getId()).isModified()) {
            throw new IllegalArgumentException();
        }
        dataGroupService.checkGroup(choice.getDataGroupList(), false, true);
    }

    // ids =============================================================================================================

    public static void setGeneratedIds(Decision decision, List<DataChoice> dataChoiceList) {
        for (DataChoice dataChoice : dataChoiceList) {
            setGeneratedIds(decision, dataChoice);
        }
    }

    public static void setGeneratedIds(Decision decision, DataChoice dataChoice) {
        dataChoice.setDecision(decision);
        dataChoice.setId(Utils.generateUUID());

        for (DataGroup dataGroup : dataChoice.getDataGroupList()) {
            dataGroup.setDataChoice(dataChoice);
            dataGroup.setId(Utils.generateUUID());

            for (DataMember dataMember : dataGroup.getDataMemberList()) {
                dataMember.setDataGroup(dataGroup);
                dataMember.setId(Utils.generateUUID());
            }
        }
    }

    public static void setGeneratedIds(DataChoice dataChoice) {
        for (DataGroup dataGroup : dataChoice.getDataGroupList()) {
            dataGroup.setDataChoice(dataChoice);
            dataGroup.setId(Utils.generateUUID());

            for (DataMember dataMember : dataGroup.getDataMemberList()) {
                dataMember.setDataGroup(dataGroup);
                dataMember.setId(Utils.generateUUID());
            }
        }
    }

    // clean objects ===================================================================================================

    /**
     * When an object is added to index n, empty Objects are created for index 0 to n-1.
     * These Objects need to be removed in order to get the collections actual size
     */
    public void removeEmptyObjects(DataChoice dataChoice) {
        List<DataGroup> dataGroupList = dataChoice.getDataGroupList();
        Iterator<DataGroup> dataGroupIterator = dataGroupList.iterator();
        while (dataGroupIterator.hasNext()) {
            DataGroup dataGroup = dataGroupIterator.next();
            List<DataMember> dataMemberList = dataGroup.getDataMemberList();
            if (dataGroup.getOffer() == null) { // hier wurde dataMemberList == null entfernt nachdem jetzt immer eine leere liste erstellt wird
                dataGroupIterator.remove();
            } else {
                dataMemberList.removeIf(member -> member == null || member.getDemander() == null);
                if (dataMemberList.isEmpty()) {
                    dataGroupIterator.remove();
                }
            }
        }
    }

    private void linkObjects(DataChoice dataChoice) {
        List<DataMember> allDataMemberList = new LinkedList<>();
        for (DataGroup dataGroup : dataChoice.getDataGroupList()) {
            dataGroup.setDataChoice(dataChoice);

            List<DataMember> dataMemberList = dataGroup.getDataMemberList();

            for (DataMember dataMember : dataMemberList) {
                dataMember.setDataGroup(dataGroup);
            }
        }
    }
}
