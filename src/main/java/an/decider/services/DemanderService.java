package an.decider.services;

import an.decider.data.api.DecisionApi;
import an.decider.data.api.DemanderApi;
import an.decider.data.dao.DecisionDao;
import an.decider.data.dao.DemanderDao;
import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import an.decider.data.model.input.Opinion;
import an.decider.web.controller.DecisionController;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class DemanderService {
    private final DemanderDao demanderDao;
    private final DemanderApi demanderApi;
    private final DecisionDao decisionDao;
    private final DecisionApi decisionApi;

    public Demander createInviteDemander(String decisionId) {
        Decision decision = decisionDao.findById(decisionId).orElseThrow(); // todo optimieren?!
        decision.setId(null);

        Demander demander = new Demander();
        demander.setDecision(decision);
        Iterable<Offer> offerList = decision.getOfferList();
        Iterator<Offer> offerIterator = offerList.iterator();
        List<Opinion> opinionList = new LinkedList<>();
        while (offerIterator.hasNext()) {
            Opinion opinion = new Opinion();

            opinion.setDemander(demander);

            Offer offer = offerIterator.next();
            opinion.setOffer(offer);

            opinionList.add(opinion);
        }
        opinionList.sort(Comparator.comparing(opinion -> opinion.getOffer().getName()));
        demander.setOpinionList(opinionList);
        return demander;
    }

    public Demander saveInviteDemander(Demander demander) {
        String decisionId = decisionApi.findIdByInviteId(demander.getDecision().getInviteId());

        demander.getDecision().setId(decisionId);

        /* todo pruefen ob alle offers bewertet wurde. Ein adnerer Nutzer koennte neue hinzugefuegt haben.
        In dem fall zurueck schicken und neue offers anzeigen (und vorhandenes speichern(?!?) */

        demander.setId(Utils.generateUUID());

        for (Opinion opinion : demander.getOpinionList()) {
            opinion.setId(Utils.generateUUID());
            opinion.setDemander(demander);
        }

        demanderApi.save(demander);

        return demander;
    }
}
