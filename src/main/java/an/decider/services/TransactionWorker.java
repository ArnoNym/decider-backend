package an.decider.services;

import an.decider.data.api.DemanderApi;
import an.decider.data.api.OfferApi;
import an.decider.data.dao.DecisionDao;
import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.OfferDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Service
public class TransactionWorker {
    /**
     * Es trat das Problem auf, dass in einer Methode mehrere Transaktionen stattfinden muessen. Dies ist der
     * beste (und einzige!?) weg den ich gefunden habe um neuer Transaktionen zu erstellen. Er basiert auf dem was
     * ich durch eine stackoverflow Antwort hier gefunden habe:
     *
     * https://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/transaction.html#transaction-programmatic
     *
     * Die @Transactional(propagation = Propagation.REQUIRES_NEW) Annotation vor Methoden reicht nicht aus, da es
     * lediglich in methoden funktioniert die public sind und von ausserhalb (was auch immer das bedeuten mag,
     * jedenfalls nicht aus meinem Java coder heraus) gecallt werden.
     */

    private final TransactionTemplate transactionTemplate;

    public TransactionWorker(PlatformTransactionManager transactionManager) {
        assert transactionManager != null : "The 'transactionManager' argument must not be null.";
        this.transactionTemplate = new TransactionTemplate(transactionManager);
    }

    public void set(Runnable transaction) {
        transactionTemplate.execute(status -> {
            transaction.run();
            return null;
        });
    }

    public <O> O get(Supplier<O> transaction) {
        return (O) transactionTemplate.execute((TransactionCallback) status -> transaction.get());
    }
}
