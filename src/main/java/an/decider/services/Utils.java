package an.decider.services;

import an.decider.data.model.IdModel;

import java.util.Collection;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Utils {
    private static int id = 0;
    public static String generateUUID() {
        String idString = ""+id; //todo loeschen
        id++;
        return UUID.randomUUID().toString().replace("-", "")+"_"+idString;
    }

    public static <M extends IdModel> void fillObjectByMatchingIds(Collection<M> from,
                                                                      Collection<M> to,
                                                                      Function<M, String> getMatchId,
                                                                      BiConsumer<M, M> setMatchingObject) {
        for (M fillInModel : to) {
            String matchId = getMatchId.apply(fillInModel);

            for (M dbModel : from) {
                String dbModelId = dbModel.getId();
                String dbMatchId = getMatchId.apply(dbModel);

                if (matchId.equals(dbMatchId)) {
                    setMatchingObject.accept(dbModel, fillInModel);
                    fillInModel.setId(dbModelId);
                    break;
                }
            }
        }
    }
}
