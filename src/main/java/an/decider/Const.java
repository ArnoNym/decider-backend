package an.decider;

public class Const {
    public static final boolean PUBLIC = true;

    public static final boolean DEVELOPER_OUT_MAX = true;
    public static final boolean DEVELOPER_OUT_MIN = false || DEVELOPER_OUT_MAX;
    public static final boolean DEVELOPER = true || DEVELOPER_OUT_MIN;

    public static final int MIN_UTILITY = 0;
    public static final int MAX_UTILITY = 10;
    public static final int DEFAULT_UTILITY = 5;
    public static final int NO_WAY_UTILITY = -1; // Has to be lower than min utility
    public static boolean isNoWayUtility(double utility) {
        return utility < 0;
    }
    public static final int UTILITY_ORDER_DECIMALS = 100;

    private static final String BEFORE = "http://";
    private static final String AFTER = PUBLIC ? "/decider" : "";
    public static final String ROOT = AFTER;
    public static final String WEBSITE = BEFORE + (PUBLIC ? "85.214.169.195:8080" : "localhost:8080") + AFTER;
}
