package an.decider.web.controller;

import an.decider.data.model.input.Decision;
import an.decider.services.DecisionService;
import an.decider.services.OfferService;
import an.decider.web.controller.global.GlobalController;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequiredArgsConstructor
public class DecisionController {
    private final OfferService offerService;
    private final DecisionService decisionService;

    @RequestMapping("/decision/saveNew")
    public ModelAndView saveNewDecision(@RequestParam String name) {
        String decisionId = decisionService.saveNew(name);

        ModelAndView mv = new ModelAndView("redirect:/decision/settings");
        mv.addObject("decisionId", decisionId);
        return mv;
    }

    @RequestMapping("/decision/settings/save")
    public ModelAndView saveSettings(Decision decision) {
        try {
            decisionService.updateSettings(decision);
        } catch (IllegalArgumentException e) {
            return GlobalController.getErrorModelView(e);
        }

        ModelAndView mv = new ModelAndView("redirect:/decision/settings");
        mv.addObject("decisionId", decision.getId());
        return mv;
    }

    @RequestMapping("/decision/overview/save")
    public String saveOverview(Decision decision, RedirectAttributes ra) {
        decisionService.saveOverview(decision);

        ra.addAttribute("decisionId", decision.getId());
        return "redirect:/decision/overview";
    }

    @RequestMapping("/decision/invite/saveOfferInviteActive")
    public ModelAndView saveOfferInviteActivity(@RequestParam String decisionId, Boolean active) {
        decisionService.updateInvite(decisionId, null, active);

        ModelAndView mv = new ModelAndView("redirect:/decision/invite");
        mv.addObject("decisionId", decisionId);
        return mv;
    }

    @RequestMapping("/decision/invite/saveDemanderInviteActive")
    public ModelAndView saveDemanderInviteActivity(@RequestParam String decisionId, Boolean active) {
        decisionService.updateInvite(decisionId, active, null);

        ModelAndView mv = new ModelAndView("redirect:/decision/invite");
        mv.addObject("decisionId", decisionId);
        return mv;
    }

    public static String inviteLinkDeactivated() {
        return "/decision/invite/inviteLinkDeactivated";
    }

}
