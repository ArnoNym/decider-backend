package an.decider.web.controller.navigation;

import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Offer;
import an.decider.services.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class NavigationController {
    
    @RequestMapping("/index")
    public ModelAndView openIndex() {
        return getIndexMv();
    }

    public static ModelAndView getIndexMv() {
        ModelAndView mv = new ModelAndView("/index");
        return mv;
    }

    @RequestMapping("/impressum")
    public ModelAndView openImpressum() {
        ModelAndView mv = new ModelAndView("/impressum");
        return mv;
    }
}
