package an.decider.web.controller.navigation;

import an.decider.Const;
import an.decider.data.api.DecisionApi;
import an.decider.data.model.out.DataChoice;
import an.decider.services.ChoiceService;
import an.decider.services.DecisionService;
import an.decider.web.controller.global.GlobalController;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Controller
@RequiredArgsConstructor
public class DecisionNavigationController {
    private final DecisionService decisionService;
    private final ChoiceService choiceService;
    private final DecisionApi decisionApi;

    // settings ========================================================================================================

    @RequestMapping("/decision/settings")
    public ModelAndView openSettings(@RequestParam String decisionId) {
        ModelAndView mv = new ModelAndView("decision/settings/settings");
        mv.addObject("decision", decisionService.getDecision(decisionId));
        mv.addObject("root", Const.ROOT);
        return mv;
    }

    // overview ========================================================================================================

    @RequestMapping("/decision/overview")
    public ModelAndView openOverview(@RequestParam String decisionId) {
        ModelAndView mv = new ModelAndView("decision/overview/overview");
        mv.addObject("decision", decisionService.getDecision(decisionId));
        mv.addObject("root", Const.ROOT);
        return mv;
    }

    // invite ==========================================================================================================

    @RequestMapping("/decision/invite")
    public ModelAndView openInvite(@RequestParam String decisionId) {
        ModelAndView mv = new ModelAndView("decision/invite/invite");

        String inviteId = decisionApi.findInviteId(decisionId);

        mv.addObject("offerLinkActive", decisionApi.isOfferLinkActiveByInviteId(inviteId));
        String offerLink = an.decider.web.Utils.createLink("/decision/invite/option?inviteId="+inviteId);
        mv.addObject("offerLink", offerLink);

        mv.addObject("demanderLinkActive", decisionApi.isDemanderLinkActiveByInviteId(inviteId));
        String demanderLink = an.decider.web.Utils.createLink("/decision/invite/participant?inviteId="+inviteId);
        mv.addObject("demanderLink", demanderLink);

        String decisionName = decisionApi.findName(decisionId);

        mv.addObject("decisionId", decisionId);
        mv.addObject("decisionName", decisionName);
        mv.addObject("root", Const.ROOT);
        return mv;
    }

    // results =========================================================================================================

    @RequestMapping("/decision/results")
    public ModelAndView openResults(@RequestParam String decisionId) {
        List<DataChoice> dataChoiceList;
        try {
            dataChoiceList = choiceService.getAll(decisionId);
        } catch (NoSuchElementException e) {
            return GlobalController.getErrorModelView("Couldn't find decision with id = "+decisionId);
        }

        List<DataChoice> modifiedDataChoiceList = new ArrayList<>();
        List<DataChoice> generatedDataChoiceList = new ArrayList<>();
        double maxUtil = 0;
        int maxUtilDataChoicesCount = 0;
        for (DataChoice dataChoice : dataChoiceList) {
            if (dataChoice.isModified()) {
                modifiedDataChoiceList.add(dataChoice);
            } else {
                generatedDataChoiceList.add(dataChoice);
                double util = dataChoice.calcUtility();
                if (util > maxUtil) {
                    maxUtil = util;
                    maxUtilDataChoicesCount = 1;
                } else if (util == maxUtil) {
                    maxUtilDataChoicesCount++;
                }
            }
        }

        ModelAndView mv = new ModelAndView("decision/choices/choices");
        mv.addObject("decisionId", decisionId);
        String decisionName = decisionService.getDecisionName(decisionId);
        mv.addObject("decisionName", decisionName);
        mv.addObject("generatedDataChoiceList", generatedDataChoiceList);
        mv.addObject("modifiedDataChoiceList", modifiedDataChoiceList);
        mv.addObject("maxUtilDataChoicesCount", maxUtilDataChoicesCount);
        mv.addObject("root", Const.ROOT);
        return mv;
    }

    // later =========================================================================================================

    @RequestMapping("/decision/later")
    public ModelAndView openLater(@RequestParam String decisionId) {
        ModelAndView mv = new ModelAndView("decision/later/later");
        mv.addObject("decisionId", decisionId);
        String decisionName = decisionService.getDecisionName(decisionId);
        mv.addObject("decisionName", decisionName);
        mv.addObject("root", Const.ROOT);
        return mv;
    }
}
