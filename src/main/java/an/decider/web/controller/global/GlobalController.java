package an.decider.web.controller.global;

import an.decider.web.controller.navigation.NavigationController;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GlobalController {
    @RequestMapping("/")
    public ModelAndView openIndex() {
        return NavigationController.getIndexMv();
    }

    @RequestMapping("/openError")
    public ModelAndView openError(@RequestParam String errorMessage) {
        return GlobalController.getErrorModelView(errorMessage);
    }

    @NotNull
    public static ModelAndView getErrorModelView(Exception e) {
        ModelAndView mv = new ModelAndView("error");
        mv.addObject("errorMessage", e.toString());
        return mv;
    }

    @NotNull
    public static ModelAndView getErrorModelView(String errorMessage) {
        ModelAndView mv = new ModelAndView("error");
        mv.addObject("errorMessage", errorMessage);
        return mv;
    }

    /*
    public ModelAndView error(HttpServletRequest httpRequest, String errorMessage) {
        if (errorMessage == null || errorMessage.equals("")) {
            errorMessage = "No error message available!";
        }
        return GlobalController.getError(errorMessage);
        /*
        ModelAndView errorPage = new ModelAndView("errorPage");
        String errorMsg = "";
        int httpErrorCode = getErrorCode(httpRequest);

        switch (httpErrorCode) {
            case 400: {
                errorMsg = "Http Error Code: 400. Bad Request";
                break;
            }
            case 401: {
                errorMsg = "Http Error Code: 401. Unauthorized";
                break;
            }
            case 404: {
                errorMsg = "Http Error Code: 404. Resource not found";
                break;
            }
            case 500: {
                errorMsg = "Http Error Code: 500. Internal Server Error";
                break;
            }
        }
        errorPage.addObject("errorMsg", errorMsg);
        *
    }
    */
}
