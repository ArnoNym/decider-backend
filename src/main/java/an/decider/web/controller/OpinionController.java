package an.decider.web.controller;

import an.decider.data.dao.DecisionDao;
import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.OfferDao;
import an.decider.data.dao.OpinionDao;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class OpinionController {
    private final DecisionDao decisionDao;
    private final DemanderDao demanderDao;
    private final OpinionDao opinionDao;
    private final OfferDao offerDao;

    public void generateAllOpinionsForOffer(@RequestParam Integer offerId) {
        //TODO
    }
}
