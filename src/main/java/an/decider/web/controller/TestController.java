package an.decider.web.controller;

import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class TestController {
    @RequestMapping("/openTest")
    public ModelAndView openTest() {
        ModelAndView mv = new ModelAndView("test");
        return mv;
    }
}
