package an.decider.web.controller.old;

import an.decider.data.DataUtils;
import an.decider.data.dao.DecisionDao;
import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.OfferDao;
import an.decider.data.dao.OpinionDao;
import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import an.decider.data.model.input.Opinion;
import an.decider.services.Utils;
import an.decider.web.model.helper.IdNameElement;
import an.decider.web.model.helper.IdNameElementList;
import an.decider.web.model.helper.controllerList.NameList;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.LinkedList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class OldOfferController {
    private OfferDao offerDao;
    private OpinionDao opinionDao;
    private DemanderDao demanderDao;
    private DecisionDao decisionDao;

    // REMOVE =========================================================================================================

    @RequestMapping("/openRemoveOffer")
    public ModelAndView openRemoveOffer(@RequestParam String decisionId) {
        IdNameElementList idNameElementList = new IdNameElementList();
        for (Offer offer : offerDao.findAllByDecisionId(decisionId)) {
            idNameElementList.add(new IdNameElement(offer.getId(), offer.getName()));
        }

        ModelAndView mv = new ModelAndView("removeOffer");
        mv.addObject("decisionId", decisionId);
        mv.addObject("idNameElementList", idNameElementList);
        return mv;
    }

    /*
    @RequestMapping("/removeOfferOpenDecision")
    public ModelAndView removeOfferOpenDecision(@RequestParam String decisionId, IdNameElementList idNameElementList) {
        List<Offer> offerList = new LinkedList<>();
        for (IdNameElement idNameElement : idNameElementList) {
            Offer offer = new Offer();
            offer.setId(idNameElement.getId());
            offerList.add(offer);
        }
        offerDao.deleteAll(offerList);

        return DecisionController.getDecisionMv(decisionId, decisionDao);
    }
    */
}
