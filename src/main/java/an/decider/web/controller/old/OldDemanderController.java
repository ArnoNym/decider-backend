package an.decider.web.controller.old;
;
import an.decider.data.DataUtils;
import an.decider.data.dao.DecisionDao;
import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.OfferDao;
import an.decider.data.dao.OpinionDao;
import an.decider.data.model.input.Decision;
import an.decider.data.model.input.Demander;
import an.decider.data.model.input.Offer;
import an.decider.data.model.input.Opinion;
import an.decider.services.Utils;
import an.decider.web.model.helper.IdNameElement;
import an.decider.web.model.helper.IdNameElementList;
import an.decider.web.model.helper.controllerList.NameList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
public class OldDemanderController {
    @Autowired
    DemanderDao demanderDao;
    @Autowired
    OfferDao offerDao;
    @Autowired
    OpinionDao opinionDao;
    @Autowired
    DecisionDao decisionDao;

    // NEW ============================================================================================================

    // EDIT ===========================================================================================================

    @RequestMapping("/demander")
    public ModelAndView openDemander(@RequestParam String demanderId) {
        Demander demander;
        Optional<Demander> demanderOptional = demanderDao.findById(demanderId);
        if (demanderOptional.isEmpty()) {
            throw new IllegalArgumentException();
        }
        demander = demanderOptional.get();

        ModelAndView mv = new ModelAndView("demander");
        mv.addObject("demander", demander);
        return mv;
    }

    @RequestMapping("/saveDemander")
    public ModelAndView saveDemander(Demander demander) {
        demanderDao.save(demander);
        return openDemander(demander.getId());
    }

    /*
    public List<Opinion> getNewOpinions(@RequestParam Long demanderId) {
        List<Long> offerIdList = offerDao.findAllOfferIdsNotInOpinionForDemander(demanderId);
        if (offerIdList.isEmpty()) {
            return new LinkedList<>();
        }
        List<Opinion> opinionList = new LinkedList<>();
        for (Long offerId : offerIdList) {
            Opinion opinion = new Opinion();
            opinion.setDemanderId(demanderId);
            opinion.setOfferId(offerId);
            opinionList.add(opinion);
        }
        return opinionList;
    }
    */

    // REMOVE =========================================================================================================

    @RequestMapping("/openRemoveDemander")
    public ModelAndView openRemoveDemander(@RequestParam String decisionId) {
        List<Demander> demanderList = demanderDao.findAllByDecisionId(decisionId);

        IdNameElementList idNameElementList = new IdNameElementList();
        for (Demander demander : demanderList) {
            idNameElementList.add(new IdNameElement(demander.getId(), demander.getName()));
        }

        ModelAndView mv = new ModelAndView("removeDemander");
        mv.addObject("decisionId", decisionId);
        mv.addObject("idNameElementList", idNameElementList);
        return mv;
    }

    /*
    @RequestMapping("/removeDemanderOpenDecision")
    public ModelAndView removeDemanderOpenDecision(@RequestParam String decisionId, IdNameElementList idNameElementList) {
        List<Demander> demanderList = new LinkedList<>();
        for (IdNameElement idNameElement : idNameElementList) {
            Demander demander = new Demander();
            demander.setId(idNameElement.getId());
            demanderList.add(demander);
        }
        demanderDao.deleteAll(demanderList);

        return DecisionController.getDecisionMv(decisionId, decisionDao);
    }
    */
    /*
    @RequestMapping("/openRemoveDemander")
    public ModelAndView openRemoveDemander(@NotNull Decision decision) {
        IdNameElementList idNameElementList = new IdNameElementList();
        for (Demander demander : decision.getDemanderList()) {
            idNameElementList.add(new IdNameElement(demander.getId(), demander.getName()));
        }

        ModelAndView mv = new ModelAndView("removeDemander");
        mv.addObject("decision", decision);
        mv.addObject("idNameElementList", idNameElementList);
        return mv;
    }

    @RequestMapping("/removeDemander")
    public ModelAndView removeDemanderOpenDecision(@RequestParam Long decisionId, IdNameElementList idNameElementList) {
        List<Demander> demanderList = new LinkedList<>();
        for (IdNameElement idNameElement : idNameElementList) {
            Demander demander = new Demander();
            demander.setId(idNameElement.getId());
            demanderList.add(demander);
        }
        demanderDao.deleteAll(demanderList);

        return decisionController.openDecision(decisionId);
    }
     */
}
