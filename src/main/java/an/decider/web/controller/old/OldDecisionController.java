package an.decider.web.controller.old;

import an.decider.data.dao.DemanderDao;
import an.decider.data.dao.OfferDao;
import an.decider.data.dao.OpinionDao;
import an.decider.data.model.input.*;
import an.decider.services.DecisionService;
import an.decider.web.controller.global.GlobalController;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
public class OldDecisionController {
    @Autowired
    DemanderDao demanderDao;
    @Autowired
    OfferDao offerDao;
    @Autowired
    OpinionDao opinionDao;
    @Autowired
    DecisionService decisionService;

    //=================================================================================================================

    @NotNull
    public ModelAndView getDecisionMv(@NotNull String decisionId) {
        Decision decision;
        try {
            decision = decisionService.getDecision(decisionId);
        } catch (NoSuchElementException e) {
            return GlobalController.getErrorModelView("Couldn't find a decision with ID " + decisionId + " in our database!");
        }
        for (Demander demander : decision.getDemanderList()) {
            demander.getOpinionList().sort(Comparator.comparing(Opinion::getId));
        }
        return getDecisionMv(decision);
    }

    @NotNull
    public static ModelAndView getDecisionMv(Decision decision) {
        ModelAndView mv = new ModelAndView("decision/decision");
        mv.addObject("decision", decision);
        return mv;
    }
}
