package an.decider.web.controller;

import an.decider.data.api.DecisionApi;
import an.decider.data.model.input.Offer;
import an.decider.services.DecisionService;
import an.decider.services.OfferService;
import an.decider.web.model.helper.saveWrapper.OfferInviteWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;

@Controller
@RequiredArgsConstructor
public class OfferController {
    private final OfferService offerService;
    private final DecisionService decisionService;
    private final DecisionApi decisionApi;

    @RequestMapping(path = { "/openNewOffer", "/decision/invite/option" })
    public ModelAndView openInviteEvent(@RequestParam("inviteId") String inviteId,
                                        @ModelAttribute("savedList") ArrayList<String> savedList) {
        if (!decisionApi.isOfferLinkActiveByInviteId(inviteId)) {
            return new ModelAndView(DecisionController.inviteLinkDeactivated());
        }

        Offer offer = offerService.createInviteOffer(inviteId);

        ModelAndView mv = new ModelAndView("/decision/invite/offer");
        mv.addObject("inviteWrapper", new OfferInviteWrapper(offer, savedList));
        return mv;
    }

    @RequestMapping(path = { "/saveNewOffer", "/decision/invite/option/save" })
    public String saveInviteEvent(OfferInviteWrapper inviteWrapper, RedirectAttributes redirectAttributes) {
        String inviteId = inviteWrapper.getCurrent().getDecision().getInviteId();

        if (inviteId == null) {
            throw new IllegalArgumentException();
        }

        if (!decisionApi.isOfferLinkActiveByInviteId(inviteId)) {
            return DecisionController.inviteLinkDeactivated();
        }

        Offer offer = inviteWrapper.getCurrent();

        offerService.saveInviteOffer(offer);

        inviteWrapper.add(offer.getName());

        redirectAttributes.addAttribute("inviteId", inviteId);
        redirectAttributes.addFlashAttribute("savedList", inviteWrapper.getSaved());
        return "redirect:/decision/invite/option";
    }
}
