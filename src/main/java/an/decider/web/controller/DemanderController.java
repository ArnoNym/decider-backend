package an.decider.web.controller;

import an.decider.data.api.DecisionApi;
import an.decider.data.api.DemanderApi;
import an.decider.data.model.input.Demander;
import an.decider.services.DecisionService;
import an.decider.services.DemanderService;
import an.decider.web.model.helper.saveWrapper.DemanderInviteWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;

@Controller
@RequiredArgsConstructor
public class DemanderController {
    private final DemanderService demanderService;
    private final DecisionService decisionService;
    private final DecisionApi decisionApi;

    @RequestMapping("/decision/invite/participant")
    public ModelAndView inviteDemander(@RequestParam("inviteId") String inviteId,
                                       @ModelAttribute("savedList") ArrayList<String> savedList) {
        if (!decisionApi.isDemanderLinkActiveByInviteId(inviteId)) {
            return new ModelAndView(DecisionController.inviteLinkDeactivated());
        }

        String decisionId = decisionApi.findIdByInviteId(inviteId);

        double maxOpinionValue = decisionApi.getMaxOpinionValue(decisionId);
        Demander demander = demanderService.createInviteDemander(decisionId);

        ModelAndView mv = new ModelAndView("/decision/invite/demander");
        mv.addObject("inviteWrapper", new DemanderInviteWrapper(demander, maxOpinionValue, savedList));
        return mv;
    }

    @RequestMapping("/decision/invite/participant/save")
    public String saveInviteDemander(DemanderInviteWrapper inviteWrapper, RedirectAttributes redirectAttributes) {
        String inviteId = inviteWrapper.getCurrent().getDecision().getInviteId();

        if (inviteId == null) {
            throw new IllegalArgumentException();
        }

        if (!decisionApi.isDemanderLinkActiveByInviteId(inviteId)) {
            return DecisionController.inviteLinkDeactivated();
        }

        Demander demander = inviteWrapper.getCurrent();

        demanderService.saveInviteDemander(demander);

        inviteWrapper.add(demander.getName());

        redirectAttributes.addAttribute("inviteId", inviteId);
        redirectAttributes.addFlashAttribute("savedList", inviteWrapper.getSaved());
        return "redirect:/decision/invite/participant";
    }

    /*

    @RequestMapping(path = { "/openNewDemander", "/decision/invite/participant" }, params = {"decisionId", "addedDemanderNameList"})
    public ModelAndView openNewDemander(@RequestParam String decisionId, NameList addedDemanderNameList) {
        Demander demander = new Demander();
        Decision decision = new Decision();
        decision.setId(decisionId);
        demander.setDecision(decision);

        Iterable<Offer> offerList = offerService.findAllByDecisionId(decisionId);
        Iterator<Offer> offerIterator = offerList.iterator();
        List<Opinion> opinionList = new LinkedList<>();
        while (offerIterator.hasNext()) {
            Opinion opinion = new Opinion();

            opinion.setDemander(demander);

            Offer offer = offerIterator.next();
            opinion.setOffer(offer);

            opinionList.add(opinion);
        }
        demander.setOpinionList(opinionList);

        // mv

        ModelAndView mv = new ModelAndView("decision/invive/demander");
        mv.addObject("addedDemanderNameList", addedDemanderNameList);
        mv.addObject("demander", demander);
        return mv;
    }

    @RequestMapping("/saveNewDemander")
    public ModelAndView saveNewDemander(Demander demander, NameList addedDemanderNameList) {
        /* todo pruefen ob alle offers bewertet wurde. Ein adnerer Nutzer koennte neue hinzugefuegt haben.
        In dem fall zurueck schicken und neue offers anzeigen (und vorhandenes speichern(?!?) *

        demanderService.saveNew(demander);

        ModelAndView mv = new ModelAndView("redirect:/openNewDemander");
        mv.addObject("decisionId", demander.getDecision().getId());
        return mv;
    }

    */
}
