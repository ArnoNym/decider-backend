package an.decider.web.controller;

import an.decider.data.model.out.*;
import an.decider.services.ChoiceService;
import an.decider.services.DecisionService;
import an.decider.web.Utils;
import an.decider.web.controller.global.GlobalController;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class ChoiceController {
    private final ChoiceService choiceService;
    private final DecisionService decisionService;

    private ModelAndView redirectChoices(String decisionId) {
        ModelAndView mv = new ModelAndView("redirect:/decision/results");
        mv.addObject("decisionId", decisionId);
        return mv;
    }

    @RequestMapping(value = {"/decision/results/modify"}, params = {"decisionId"})
    public ModelAndView openModifyChoice(@RequestParam("decisionId") String decisionId) {
        ModelAndView mv = new ModelAndView("decision/choices/modifyChoice/modifyChoice");
        mv.addObject("decisionId", decisionId);
        String decisionName = decisionService.getDecisionName(decisionId);
        mv.addObject("decisionName", decisionName);
        mv.addObject("dataChoice", choiceService.createModifyChoice(decisionId));
        mv.addObject("overrideAllowed", false);
        return mv;
    }

    @RequestMapping(value = {"/decision/results/modify"}, params = {"decisionId", "dataChoiceId"})
    public ModelAndView openModifyChoice(@RequestParam("decisionId") String decisionId,
                                         @RequestParam("dataChoiceId") String dataChoiceId) {
        DataChoice dataChoice = choiceService.findModifyChoice(dataChoiceId);
        ModelAndView mv = new ModelAndView("decision/choices/modifyChoice/modifyChoice");
        mv.addObject("decisionId", decisionId);
        String decisionName = decisionService.getDecisionName(decisionId);
        mv.addObject("decisionName", decisionName);
        mv.addObject("dataChoice", dataChoice);
        mv.addObject("overrideAllowed", dataChoice.isModified());
        return mv;
    }

    @RequestMapping("/decision/results/modify/saveNew")
    public ModelAndView saveNewChoice(DataChoice dataChoice) {
        try {
            choiceService.save(dataChoice, false);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return GlobalController.getErrorModelView(e); // todo popup, dass sagt, dass die groesse nicht passt
        }
        return redirectChoices(dataChoice.getDecision().getId());
    }

    @RequestMapping("/decision/results/modify/update")
    public ModelAndView updateChoice(DataChoice dataChoice) {
        try {
            choiceService.save(dataChoice, true);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return GlobalController.getErrorModelView(e); // todo popup, dass sagt, dass die groesse nicht passt
        }
        return redirectChoices(dataChoice.getDecision().getId());
    }

    @RequestMapping("/decision/results/select")
    public ModelAndView openSelectChoice(@RequestParam String decisionId, @RequestParam String dataChoiceId) {
        ModelAndView mv = new ModelAndView("decision/choices/selectChoice/selectChoice");
        mv.addObject("decisionId", decisionId);
        String decisionName = decisionService.getDecisionName(decisionId);
        mv.addObject("decisionName", decisionName);
        String link = Utils.createLink("/decision/results/selected?choiceId="+dataChoiceId);
        mv.addObject("link", link);
        mv.addObject("dataChoice", choiceService.find(dataChoiceId));
        return mv;
    }

    @RequestMapping("/decision/results/selected")
    public ModelAndView openSelectedChoice(@RequestParam String choiceId) {
        ModelAndView mv = new ModelAndView("decision/choices/selectChoice/selectedChoice");
        String decisionName = decisionService.getDecisionNameByChoiceId(choiceId);
        mv.addObject("decisionName", decisionName);
        mv.addObject("dataChoice", choiceService.find(choiceId));
        return mv;
    }

    /*
    @RequestMapping("/choices")
    public ModelAndView openChoices(String decisionId) {
        List<DataChoice> dataChoiceList;
        try {
            dataChoiceList = choiceService.getOrCreateAndSaveChoices(decisionId);
        } catch (NoSuchElementException e) {
            return GlobalController.getError("Couldn't find decision with id = "+decisionId);
        }

        List<DataChoice> modifiedDataChoiceList = new ArrayList<>();
        List<DataChoice> generatedDataChoiceList = new ArrayList<>();
        double maxUtil = 0;
        int maxUtilDataChoicesCount = 0;
        for (DataChoice dataChoice : dataChoiceList) {
            if (dataChoice.isModified()) {
                modifiedDataChoiceList.add(dataChoice);
            } else {
                generatedDataChoiceList.add(dataChoice);
                double util = dataChoice.calcUtility();
                if (util > maxUtil) {
                    maxUtil = util;
                    maxUtilDataChoicesCount = 1;
                } else if (util == maxUtil) {
                    maxUtilDataChoicesCount++;
                }
            }
        }

        ModelAndView mv = new ModelAndView("results");
        mv.addObject("decisionId", decisionId);
        mv.addObject("generatedDataChoiceList", generatedDataChoiceList);
        mv.addObject("modifiedDataChoiceList", modifiedDataChoiceList);
        mv.addObject("maxUtilDataChoicesCount", maxUtilDataChoicesCount);
        return mv;
    }
    */
}
