package an.decider.web.model.helper.saveWrapper;

import java.util.ArrayList;
import java.util.List;

public class InviteWrapper<N, S> {
    private N current;
    private List<S> saved = new ArrayList<>();

    public InviteWrapper() {
    }

    public InviteWrapper(N current, List<S> saved) {
        this.current = current;
        this.saved = saved;
    }

    public void add(S s) {
        saved.add(s);
    }

    public boolean isEmpty() {
        return saved.isEmpty();
    }

    public int size() {
        return saved.size();
    }

    public N getCurrent() {
        return current;
    }

    public void setCurrent(N current) {
        this.current = current;
    }

    public List<S> getSaved() {
        return saved;
    }

    public void setSaved(List<S> saved) {
        this.saved = saved;
    }
}
