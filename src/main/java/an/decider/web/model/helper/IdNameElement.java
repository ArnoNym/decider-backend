package an.decider.web.model.helper;

public class IdNameElement {
    private String id;
    private String name = null;

    public IdNameElement() {
    }

    public IdNameElement(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
