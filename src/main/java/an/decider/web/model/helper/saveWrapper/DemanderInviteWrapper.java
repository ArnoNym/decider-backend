package an.decider.web.model.helper.saveWrapper;

import an.decider.data.model.input.Demander;

import java.util.List;

public class DemanderInviteWrapper extends InviteWrapper<Demander, String> {
    private Double maxOpinionValue = null;

    public DemanderInviteWrapper() {
        super();
    }

    public DemanderInviteWrapper(Demander next, double maxOpinionValue, List<String> saved) {
        super(next, saved);
        this.maxOpinionValue = maxOpinionValue;
    }

    @Override
    public void add(String s) {
        super.add(s);
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public int size() {
        return super.size();
    }

    @Override
    public Demander getCurrent() {
        return super.getCurrent();
    }

    @Override
    public void setCurrent(Demander current) {
        super.setCurrent(current);
    }

    @Override
    public List<String> getSaved() {
        return super.getSaved();
    }

    @Override
    public void setSaved(List<String> saved) {
        super.setSaved(saved);
    }

    public Double getMaxOpinionValue() {
        return maxOpinionValue;
    }

    public void setMaxOpinionValue(Double maxOpinionValue) {
        this.maxOpinionValue = maxOpinionValue;
    }
}
