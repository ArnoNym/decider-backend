package an.decider.web.model.helper.saveWrapper;

import an.decider.data.model.input.Offer;

import java.util.List;

public class OfferInviteWrapper extends InviteWrapper<Offer, String> {
    /*
    Alles ueberschrieben damit IntelliJ das in der html thymleaf datei nicht rot markiert
    */

    public OfferInviteWrapper() {
        super();
    }

    public OfferInviteWrapper(Offer next, List<String> saved) {
        super(next, saved);
    }

    @Override
    public void add(String s) {
        super.add(s);
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public int size() {
        return super.size();
    }

    @Override
    public Offer getCurrent() {
        return super.getCurrent();
    }

    @Override
    public void setCurrent(Offer current) {
        super.setCurrent(current);
    }

    @Override
    public List<String> getSaved() {
        return super.getSaved();
    }

    @Override
    public void setSaved(List<String> saved) {
        super.setSaved(saved);
    }
}
