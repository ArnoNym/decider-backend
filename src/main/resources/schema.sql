
CREATE TABLE decision (
    id VARCHAR(255) NOT NULL,
    invite_id VARCHAR(255) NOT NULL UNIQUE,
    version BIGINT NOT NULL,
    last_used_time TIMESTAMP,
    created_time TIMESTAMP,
	name VARCHAR(50) NOT NULL,
    password VARCHAR(50),
    add_demander_bonus DOUBLE,
    max_opinion_value INT NOT NULL,
    note VARCHAR(500),
    offer_link_active BOOLEAN NOT NULL,
    demander_link_active BOOLEAN NOT NULL,

    PRIMARY KEY(id)
);

CREATE TABLE demander (
    id VARCHAR(255) NOT NULL,
	decision_id VARCHAR(255) NOT NULL,
	name VARCHAR(50) NOT NULL,
    has_to_be_included BOOLEAN NOT NULL,

    PRIMARY KEY(id),
    FOREIGN KEY(decision_id) REFERENCES decision(id) ON DELETE CASCADE
);

CREATE TABLE offer (
    id VARCHAR(255) NOT NULL,
	decision_id VARCHAR(255) NOT NULL,
	Name VARCHAR(50) NOT NULL,
    has_to_be_included BOOLEAN NOT NULL,
    available INT,
    min_group_size INT NOT NULL,
    max_group_size INT,
    Note VARCHAR(500),

    PRIMARY KEY(id),
    FOREIGN KEY(decision_id) REFERENCES decision(id) ON DELETE CASCADE
);

CREATE TABLE opinion (
	id VARCHAR(255) NOT NULL,
	demander_id VARCHAR(255) NOT NULL,
	offer_id VARCHAR(255) NOT NULL,
    value DOUBLE,
    note VARCHAR(500),

    PRIMARY KEY(id),
    FOREIGN KEY(demander_id) REFERENCES demander(id) ON DELETE CASCADE,
    FOREIGN KEY(offer_id) REFERENCES offer(id) ON DELETE CASCADE
);

CREATE TABLE data_choice (
	id VARCHAR(255) NOT NULL,
	decision_id VARCHAR(255) NOT NULL,
	modified BOOLEAN NOT NULL,

    PRIMARY KEY(id),
    FOREIGN KEY(decision_id) REFERENCES decision(id) ON DELETE CASCADE
);

CREATE TABLE data_group (
	id VARCHAR(255) NOT NULL,
	data_choice_id VARCHAR(255) NOT NULL,
	offer_id VARCHAR(255) NOT NULL,

    PRIMARY KEY(id),
    FOREIGN KEY(data_choice_id) REFERENCES data_choice(id) ON DELETE CASCADE,
    FOREIGN KEY(offer_id) REFERENCES offer(id) ON DELETE CASCADE
);

CREATE TABLE data_member (
	id VARCHAR(255) NOT NULL,
	demander_id VARCHAR(255) NOT NULL,
	data_group_id VARCHAR(255) NOT NULL,

    PRIMARY KEY(id),
    FOREIGN KEY(demander_id) REFERENCES demander(id) ON DELETE CASCADE,
    FOREIGN KEY(data_group_id) REFERENCES data_group(id) ON DELETE CASCADE
);