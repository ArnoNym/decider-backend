

INSERT INTO Decision (id, invite_id, version, last_used_time, created_time, name, password,
add_demander_bonus, max_opinion_value, note, offer_link_active, demander_link_active) VALUES
('11', 'i11', 1, '2000-01-01 00:00:00', '2000-01-01 00:00:00', 'BrettspielAG Essen', NULL, 10, 10, NULL, false, false),
('12', 'i12', 1, '2000-01-01 00:00:00', '2000-01-01 00:00:00', 'BrettspielAG Spielen', NULL, 10, 10, NULL, false, false);

INSERT INTO Demander (id, decision_id, name, has_To_Be_Included) VALUES
('11', '11', 'Tom', FALSE),
('12', '11', 'Tim', FALSE),
('13', '11', 'Rolph', FALSE),
('14', '11', 'Martin', FALSE),

(15, 12, 'Magi', FALSE)/*,
(16, 12, 'Jana', FALSE)*/;

INSERT INTO Offer (Id, Decision_Id, Name, Has_To_Be_Included, Available, Min_Group_Size, Max_Group_Size, Note) VALUES
(11, 11, 'Schach', FALSE, 1, 1, null, ''),
(12, 11, 'Risiko', FALSE, 2, 1, 1, ''),
(13, 11, 'Carcassone', FALSE, 1, 0, null, ''),

(14, 12, 'Kochen', FALSE, 1, 0, null, ''),
(15, 12, 'Ballet', FALSE, 1, 1, null, '');

INSERT INTO Opinion (Id, Demander_Id, Offer_Id, Value, Note) VALUES
(1, 11, 11, 5, ''),
(2, 11, 12, 9, ''),
(3, 11, 13, 7, ''),
(4, 12, 11, 9, ''),
(5, 12, 12, 9, ''),
(6, 12, 13, 4, ''),
(7, 13, 11, 5, ''),
(8, 13, 12, 9, ''),
(9, 13, 13, 7, ''),
(10, 14, 11, 5, ''),
(11, 14, 12, 9, ''),
(12, 14, 13, 7, ''),

(13, 15, 14, 5, '')/*,
(15, 15, 5, ''),
(16, 14, 5, ''),
(16, 15, 5, '')*/;


/*
INSERT INTO Decision VALUES (11, 'BrettspielAG Essen', NULL , 1, 2, NULL);

INSERT INTO Decision VALUES (11, 'BrettspielAG Essen', NULL , 1, 2, NULL);

INSERT INTO Decision (id, name, password, min_Group_Count, max_Group_Count, note) VALUES
(11, 'BrettspielAG Essen', NULL , 1, 2, NULL),
(12, 'BrettspielAG Essen', NULL , 1, 2, NULL);

INSERT INTO Decision VALUES ('BrettspielAG Essen', NULL, 1, 1234);
INSERT INTO Decision VALUES ('BrettspielAG Spiele', 3, 1, 1234);

INSERT INTO Demander VALUES (1, TRUE, 'Peter');
INSERT INTO Demander VALUES (2, TRUE, 'Hans');

INSERT INTO Offer VALUES (1, 1, 5, 2, 'Risiko', '');
INSERT INTO Offer VALUES (2, 1, 2, 2, 'Schach', '');
*/

/*
INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Peter', 'Risiko', 8);
INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Peter', 'Schach', 7);

INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Hans', 'Risiko', 4);
INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Hans', 'Schach', 3);
*/