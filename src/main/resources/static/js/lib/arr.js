function countSmallerValues(arr, v) {
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < v) count++;
        else break;
    }
    return count;
}

function spliceValue(arr, v) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === v) {
            arr.splice(i, 1);
            break;
        }
    }
}