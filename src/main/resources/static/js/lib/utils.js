function getFirstChildByClassname(element, classname) {
    for (let i = 0; i < element.children.length; i++) {
        const child = element.children[i];
        if (child.classList.contains(classname)) {
            return child;
        }
    }
    return null;
}