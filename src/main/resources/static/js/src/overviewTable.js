/** Abstände zu dem eigentlichen Inhalt der Tabelle, also den Opinions */

const TOP_PADDING = 6;

const BOTTOM_PADDING = 1;

const TOP_ROW__LEFT_PADDING = 2;
const HEAD_ROWS__LEFT_PADDING = 1;
const LEFT_PADDING = 3;

const TOP_ROW__RIGHT_PADDING = 1;
const RIGHT_PADDING = 0;


const OFFER_INPUT_CLASS = "offerInput";
const DEMANDER_INPUT_CLASS = "demanderInput";
const OPINION_INPUT_CLASS = "opinionInput";

/**
 * At creation every offer and demanders index relative to the table gets added to these Lists.
 * These Indexes also represent the position the decisions List
 */
let offerIndices = [];
let demanderIndices = [];
let opinionIndex = 0;
let maxOpinionValue;


function init(maxOpinionValue, demanderListSize, offerListSize) {
    window.maxOpinionValue = maxOpinionValue;
    for (let i = 0; i < demanderListSize; i++) {
        demanderIndices.push(i);
    }
    for (let i = 0; i < offerListSize; i++) {
        offerIndices.push(i);
    }
}

function addOffer() {
    const table = document.getElementById('modifyChoiceMainTable');
    const offerIndex = getNextIndex(offerIndices);
    addOfferCells(table.rows, offerIndex);
    for (let i = 0; i < demanderIndices.length; i++) {
        addOpinionCell(table.rows[i + TOP_PADDING], demanderIndices[i], offerIndex);
    }
    offerIndices.push(offerIndex);
}

function addDemander() {
    const table = document.getElementById('modifyChoiceMainTable');
    const demanderIndex = getNextIndex(demanderIndices);
    const row = table.insertRow(table.rows.length - BOTTOM_PADDING);
    addDemanderCells(row, demanderIndex);
    for (let i = 0; i < offerIndices.length; i++) {
        addOpinionCell(row, demanderIndex, offerIndices[i]);
    }
    demanderIndices.push(demanderIndex);
}

function addDemanderCells(row, demanderIndex) {
    addDeleteCell(row, undefined, "deleteDemander("+demanderIndex+");");
    let cell = addCell(row, undefined, DEMANDER_INPUT_CLASS, "text", "demanderList["+demanderIndex+"].name", "",true);
    addHidden(cell, "demanderList["+demanderIndex+"].index", demanderIndex.toString());
    cell = addCell(row, undefined, null, "checkbox", "demanderList["+demanderIndex+"].hasToBeIncluded", "true");
    addHidden(cell, "_demanderList["+demanderIndex+"].hasToBeIncluded", "on");
}

function addOfferCells(rows, offerIndex) {
    const index = rows[0].cells.length - 1;
    console.log(index);
    addDeleteCell(rows[0], index, "deleteOffer("+offerIndex+");");

    let cell = addCell(rows[1], undefined, OFFER_INPUT_CLASS, "text", "offerList["+offerIndex+"].name", "", true);
    addHidden(cell, "offerList["+offerIndex+"].index", offerIndex.toString());

    cell = addCell(rows[2], undefined, OFFER_INPUT_CLASS, "number", "offerList["+offerIndex+"].available", "1");
    cell.firstElementChild.setAttribute("min", "1");
    cell.firstElementChild.setAttribute("placeholder", "infinity");

    cell = addCell(rows[3], undefined, OFFER_INPUT_CLASS, "number", "offerList["+offerIndex+"].minGroupSize", "1", true);
    cell.firstElementChild.setAttribute("min", "1");

    cell = addCell(rows[4], undefined, OFFER_INPUT_CLASS, "number", "offerList["+offerIndex+"].maxGroupSize");
    cell.firstElementChild.setAttribute("min", "1");
    cell.firstElementChild.setAttribute("placeholder", "infinity");

    cell = addCell(rows[5], undefined, null, "checkbox", "offerList["+offerIndex+"].hasToBeIncluded", "true");

    addHidden(cell, "_offerList["+offerIndex+"].hasToBeIncluded", "on");
}

function addOpinionCell(row, demanderIndex, offerIndex) {
    let cell = addCell(row, undefined, OPINION_INPUT_CLASS, "number", "demanderList["+demanderIndex+"].opinionList["+offerIndex+"].value", "", true);
    cell.firstElementChild.setAttribute("min", "-1");
    cell.firstElementChild.setAttribute("max", maxOpinionValue);
    addHidden(cell,"demanderList["+demanderIndex+"].opinionList["+offerIndex+"].index", opinionIndex.toString());
    addHidden(cell,"demanderList["+demanderIndex+"].opinionList["+offerIndex+"].offer.index", offerIndex.toString());
    opinionIndex++;
}

/**
 * Wird als String von Thymeleaf erstellt, weshalb IntelliJ die Verwendung nicht erkennt
 */
function deleteOffer(offerIndex) {
    const table = document.getElementById('modifyChoiceMainTable');
    const tableOfferIndex = countSmallerValues(offerIndices, offerIndex);

    for (let i = table.rows.length - BOTTOM_PADDING - 1; i >= TOP_PADDING - 1; i--) {
        table.rows[i].deleteCell(tableOfferIndex + LEFT_PADDING);
    }
    for (let i = TOP_PADDING - 1 - 1; i >= 1; i--) {
        table.rows[i].deleteCell(tableOfferIndex + LEFT_PADDING - 2);
    }
    table.rows[0].deleteCell(tableOfferIndex + LEFT_PADDING - 1);

    spliceValue(offerIndices, offerIndex);
}

/**
 * Wird als String von Thymeleaf erstellt, weshalb IntelliJ die Verwendung nicht erkennt
 */
function deleteDemander(demanderIndex) {
    const table = document.getElementById('modifyChoiceMainTable');
    const tableDemanderIndex = countSmallerValues(demanderIndices, demanderIndex) + TOP_PADDING;
    table.deleteRow(tableDemanderIndex);
    spliceValue(demanderIndices, demanderIndex);
}

//=====================================================================================================================

function addCell(row, index = -1, classes = null, type, name, value = "", required = false) {
    let cell = row.insertCell(index);
    let box = document.createElement("input");
    if (classes != null) {
        box.setAttribute("class", classes);
    }
    box.setAttribute("type", type);
    box.setAttribute("name", name);
    box.setAttribute("value", value);
    if (required) {
        box.required = true;
    }
    cell.appendChild(box);
    return cell;
}

function addHidden(cell, name, value) {
    let box = document.createElement("input");
    box.setAttribute("type", "hidden");
    box.setAttribute("name", name);
    box.setAttribute("value", value);
    cell.appendChild(box);
    return cell;
}

function addDeleteCell(row, index, onclick) {
    let cell = row.insertCell(index);
    let box = document.createElement("input");
    box.setAttribute("type", "button");
    box.setAttribute("value", "X");
    box.setAttribute("onclick", onclick);
    box.setAttribute("style", "width:100%;");
    cell.appendChild(box);
    return cell;
}

// =====================================================================================================================

function getNextIndex(arr) {
    let v;
    if(arr.length === 0) {
        v = 0;
    } else {
        v = Math.max.apply(null, arr) + 1;
    }
    return v;
}

