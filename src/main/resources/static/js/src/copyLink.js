function copy(linkId) {
    /* Get the text field */
    const link = document.getElementById(linkId);

    /* Select the text field */
    link.select();
    link.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    document.execCommand("copy");
}