// declaration =========================================================================================================

const TABLE_HEADER_ROWS = 1;
const UNUSED = "unused";

const MAIN_TABLE_ID = "modifyChoiceMainTable";
const UNUSED_TABLE_ID = "modifyChoiceUnusedTable";

const SPACE_CLASS = "space";
const SPACE_EMPTY_CLASS = "empty";
const SPACE_FULL_CLASS = "full";
const SPACE_HOVERED_CLASS = "hovered";
const SPACE_HOVERED_NO_WAY_CLASS = "hoveredNoWay";

const ENTITY_CLASS = "entity";
const ENTITY_HOLD_CLASS = "hold";

const CONTENT_CLASS = "content";

const NAME_CLASS = "name";
const INPUT_FIELD_CLASS = "inputField"
const OPINION_DIFF_CLASS = "opinionDiff";
const DEMANDER_ID_CLASS = "demanderId";

const OFFER_ID_CLASS = "offerId";

// construct ===========================================================================================================

const unusedTable = document.getElementById(UNUSED_TABLE_ID);
const unusedTableWrapper = new UnusedTableWrapper(unusedTable);

const mainTable = document.getElementById(MAIN_TABLE_ID);
const mainTableWrapper = new MainTableWrapper(mainTable);

const entityArr = document.querySelectorAll("."+ENTITY_CLASS);
const contentArr = document.querySelectorAll("."+CONTENT_CLASS);
const spaceArr = document.querySelectorAll("."+SPACE_CLASS);
const spaceFullArr = document.querySelectorAll("."+SPACE_FULL_CLASS);
const spaceEmptyArr = document.querySelectorAll("."+SPACE_EMPTY_CLASS);

for (const entity of entityArr) {
    entity.addEventListener('dragstart', dragStart);
    entity.addEventListener('dragend', dragEnd);
}
for (const full of spaceFullArr) {
    SPACES.addFullEvents(full);
}
for (const empty of spaceEmptyArr) {
    SPACES.addEmptyEvents(empty);
}

let opinionArray = []; // filled in html page

let addDemanderBonus; // filled in html page

// =====================================================================================================================

let draggingValidObject = false;

/*todo drag und drop muss auf tauchscreen gesondert erstellt werden https://www.mediaevent.de/javascript/drag-and-drop.html */


