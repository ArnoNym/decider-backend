const TABLE = {
    getInputField(rowIndex, colIndex) {
        let inputFieldId;
        if(colIndex == null) {
            MAIN_TABLE.createInputFieldId(rowIndex, colIndex);
        } else {
            UNUSED_TABLE.createInputFieldId(rowIndex);
        }
        return document.getElementById(inputFieldId);
    },
    getRowIndex(table, cell, nullable = false) {
        const rowArray = table.rows;
        let rowIndex;
        for (let i = 0;  i < rowArray.length; i++) {
            const row = rowArray[i];
            if (row.contains(cell)) {
                rowIndex = i;
                break;
            }
        }
        if (!nullable && rowIndex == null) throw "Could not find rowIndex!";
        return rowIndex;
    }
}

class MainTableWrapper {
    constructor(mainTable) {
        this.table = mainTable;
        this.spaces = mainTable.querySelectorAll("."+SPACE_CLASS);
    }
    getOfferIdByIndex(colIndex) {
        const headerCell = this.table.rows[0].cells[colIndex];
        return getFirstChildByClassname(headerCell, OFFER_ID_CLASS).value;
    }

    getSpaces() {
        return this.spaces;
    }
    getSpacesByColIndex(colIndex) {
        let spaces = [];
        const rows = this.table.rows;
        for (let i = 1; i < rows.length; i++) {
            const row = rows[i];
            const space = row.cells[colIndex];
            if (space == null) {
                break;
            }
            spaces[i - 1] = space;
        }
        return spaces;
    }
    getRowIndex(cell, nullable = false) {
        return TABLE.getRowIndex(this.table, cell, nullable);
    }
    getColIndex(cell, nullable = false) {
        const rowIndex = this.getRowIndex(cell, nullable);
        if (rowIndex == null) return null;
        const row = this.table.rows[rowIndex];
        return MAIN_TABLE.getColIndex(row, cell, nullable);
    }
}
const MAIN_TABLE = {
    offerIndexToColIndex(offerIndex) {
        return offerIndex;
    },
    colIndexToOfferIndex(colIndex) {
        return colIndex;
    },
    demanderIndexToRowIndex(demanderIndex) {
        return demanderIndex + 1;
    },
    rowIndexToDemanderIndex(rowIndex) {
        return rowIndex - 1;
    },
    getColIndex(row, cell, nullable = false) {
        let colIndex;
        for (let i = 0;  i < row.cells.length; i++) {
            if (row.cells[i] === cell) {
                colIndex = i;
                break;
            }
        }
        if (!nullable && colIndex == null) throw "Could not find colIndex!";
        return colIndex;
    },
    createInputFieldId(colIndex, rowIndex) {
        return INPUT_FIELD_CLASS + "_" + rowIndex + "_" + colIndex;
    }
}

class UnusedTableWrapper {
    constructor(unusedTable) {
        this.table = unusedTable;
        this.spaces = unusedTable.querySelectorAll("."+SPACE_CLASS);
    }
    getSpaces() {
        return this.spaces;
    }
    getRowIndex(cell, nullable = false) {
        return TABLE.getRowIndex(this.table, cell, nullable);
    }
}
let UNUSED_TABLE = {
    createInputFieldId(rowIndex) {
        return INPUT_FIELD_CLASS + "_" + UNUSED + "_" + rowIndex;
    }
}