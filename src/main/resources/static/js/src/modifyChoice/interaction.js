// drag start/end ======================================================================================================

function dragStart(event) {
    const object = event.target; // == this

    if (!object.classList.contains(ENTITY_CLASS)) return;

    object.classList.add(ENTITY_HOLD_CLASS);

    event.dataTransfer.setData("text", object.id);

    //setTimeout(() => (object.classList.add('invisible')), 0);

    draggingValidObject = true;
}

function dragEnd() {
    this.classList.remove(ENTITY_HOLD_CLASS);
    draggingValidObject = false;
}

// drag leave/enter ====================================================================================================

function dragLeaveFull(event) {
    if (!draggingValidObject) return;
    if (isInnerDrag(event)) return;
    this.classList.remove(SPACE_HOVERED_CLASS);
    this.classList.remove(SPACE_HOVERED_NO_WAY_CLASS);
}
function dragLeaveEmpty(event) {
    if (!draggingValidObject) return;
    if (isInnerDrag(event)) return;
    this.classList.remove(SPACE_HOVERED_CLASS);
    this.classList.remove(SPACE_HOVERED_NO_WAY_CLASS);
}

function dragEnterFull(event) {
    dragEnter.call(this, event);
}
function dragEnterEmpty(event) {
    dragEnter.call(this, event);
}
function dragEnter(event) {
    if (!draggingValidObject) return false;
    if (isInnerDrag(event)) return false;

    const space = this;

    if (SPACES.isNoWaySpace(space)) {
        space.classList.add(SPACE_HOVERED_NO_WAY_CLASS);
        return false;
    }

    event.preventDefault();
    space.classList.add(SPACE_HOVERED_CLASS);
    return true;
}

/**
 *  In Kombination mit 'pointer-events: none;' fuehr es dazu, dass die Input Elemente nicht leave und enter ausloesen
 *  und damit die 'hover' classe und damit die Markierung entfernen'
 */
function isInnerDrag(event) {
    return event.currentTarget.contains(event.relatedTarget);
}

// drag over ===========================================================================================================

function dragOver(event) {
    if (!draggingValidObject) return;
    event.preventDefault();
}

// drag drop ===========================================================================================================

function dragDropEmpty(event) {
    dragDrop.call(this, event, false);
}
function dragDropFull(event) {
    dragDrop.call(this, event, true);
}
function dragDrop(event, bothFull) {
    if (!draggingValidObject) return;

    event.preventDefault();

    const dragToSpace = this;
    const dragFromSpaceDemander = document.getElementById(event.dataTransfer.getData("text"));

    if (dragFromSpaceDemander == null) {
        denyDrop_becauseDroppedOnItself(dragToSpace)
        return;
    }

    const dragFromSpace = dragFromSpaceDemander.parentElement;
    const dragToSpaceDemander = dragToSpace.removeChild(dragToSpace.lastElementChild);
    const dragToSpaceValueDiff = ENTITIES.getValueDiff(dragToSpaceDemander);

    if (SPACES.isNoWaySpace(null, dragToSpaceValueDiff)) {
        denyDrop_becauseNoWayValue(dragToSpace, dragToSpaceDemander);
        return;
    }

    if (bothFull) {
        SPACES.setFull(dragFromSpace, false);
        SPACES.setFull(dragToSpace, false);
    } else {
        SPACES.setEmpty(dragFromSpace);
        SPACES.setFull(dragToSpace, true);
    }

    dragToSpace.append(dragFromSpaceDemander);
    dragFromSpace.append(dragToSpaceDemander);

    SPACES.updateJava(dragFromSpace);
    SPACES.updateJava(dragToSpace);

    OPINIONS.addToValueSum(dragToSpaceValueDiff);

    activateMouseOver.call(dragToSpace);
}
function denyDrop_becauseDroppedOnItself(space) {
    SPACES.setFull(space, false);
}
function denyDrop_becauseNoWayValue(dragToSpace, dragToSpaceDemander) {
    if (ENTITIES.isEmpty(dragToSpaceDemander)) {
        SPACES.setEmpty(dragToSpace);
        dragToSpace.append(dragToSpaceDemander);
        SPACES.resetAllValueDiffs()
        SPACES.setAllMaxValueDiffs();
    } else {
        SPACES.setFull(dragToSpace, false);
        dragToSpace.append(dragToSpaceDemander);
        activateMouseOver.call(dragToSpace);
    }
}

// mouse over/leave ====================================================================================================

let lock = false;

function activateMouseOver() {
    mouseOverFull.call(this);
    lock = true;
}

function mouseOverFull() {
    if (lock) {
        console.log("mouseOver lock");
        lock = false;
        return;
    }
    SPACES.setAllValueDiffs(this);
}
function mouseLeaveFull() {
    if (lock) {
        console.log("mouseLeave lock");
        return;
    }
    console.log("mouseLeave");

    SPACES.resetAllValueDiffs();
    SPACES.setAllMaxValueDiffs();
}

// =====================================================================================================================

/**
 * Veraendere das ID Feld der Objecte = DataMember so, dass sie beim Erstellen der Java Datei in der DataMemberList
 * der neuen DataGroup landen. Im Controller erhalten sie dann die entsprechende DataGroup.id.
 * Weitere Felder muessen nicht uebergeben werden, da eine spezielle Update Query verwendet wird um die Aenderungen
 * in die Datenbank zu uebertragen
 */
/*
function updateJavaInObjects(oldSpace, currentSpace, bothFull = false) {
    if (oldSpace === undefined || currentSpace === undefined) throw "Spaces may not be undefined."

    const rowArray = mainTable.rows;

    if (!SPACES.isEmpty(oldSpace)) {
        const entity = SPACES.getEntity(oldSpace);
        const oldRowIndex = mainTableWrapper.getRowIndex(oldSpace);
        const oldDemanderIndex = MAIN_TABLE.rowIndexToDemanderIndex(oldRowIndex);
        const oldColIndex = MAIN_TABLE.getColIndex(rowArray[oldRowIndex], oldSpace);
        const oldInputField = TABLE.getInputField(oldDemanderIndex, oldColIndex);
        if (oldInputField === null) throw "oldInputField may not be undefined.";
    }

    const newRowIndex = mainTableWrapper.getRowIndex(currentSpace);
    const newDemanderIndex = MAIN_TABLE.rowIndexToDemanderIndex(newRowIndex);
    let newColIndex = MAIN_TABLE.getColIndex(rowArray[newRowIndex], currentSpace);
    if (newColIndex == null) newColIndex = -1;
    if (bothFull) {
        const newInputField = TABLE.getInputField(newDemanderIndex, newColIndex);
        if (newInputField === null) throw "newInputField may not be undefined.";

        oldInputField.id = "myPlaceholder"; // Is not a key or anything. Just to make sure that two objects never have the same id

        newInputField.id = INPUT_FIELD_CLASS+"_"+oldDemanderIndex+"_"+oldColIndex;
        newInputField.name = "dataGroupList["+oldColIndex+"].dataMemberList["+oldDemanderIndex+"].demander.id";
    }
    oldInputField.id = INPUT_FIELD_CLASS+"_" + newDemanderIndex + "_" + newColIndex;
    oldInputField.name = "dataGroupList[" + newColIndex + "].dataMemberList[" + newDemanderIndex + "].demander.id";
}
*/