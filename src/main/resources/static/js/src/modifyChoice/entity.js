const ENTITIES = {
    isEmpty(entity) {
        return ENTITIES.getDemanderId(entity, true) == null;
    },
    getDemanderId(entity, nullable = false) {
        const demanderIdDiv = getFirstChildByClassname(entity, DEMANDER_ID_CLASS);
        if (demanderIdDiv == null) {
            if (nullable) {
                return null;
            } else {
                throw "demanderIdDiv is null!"
            }
        }
        return demanderIdDiv.value;
    },
    getValueDiff(entity) {
        return getFirstChildByClassname(entity, OPINION_DIFF_CLASS).innerHTML;
    },
    getSpace(entity) {
        return entity.parentElement;
    },
    getOfferId(entity, nullable = false) {
        return SPACES.getOfferId(ENTITIES.getSpace(entity), nullable);
    },
    calcValue(entity, nullable = false) {
        const demanderId = ENTITIES.getDemanderId(entity, nullable);
        const offerId = ENTITIES.getOfferId(entity, nullable);
        return OPINIONS.getValue(demanderId, offerId);
    },
    resetValueDiff(entity) {
        const mainTableInputInfo = getFirstChildByClassname(entity, OPINION_DIFF_CLASS);
        mainTableInputInfo.innerHTML = "";
    },
    setValueDiff(entity, valueDiff) {
        const mainTableInputInfo = getFirstChildByClassname(entity, OPINION_DIFF_CLASS);
        mainTableInputInfo.innerHTML = valueDiff;
    },
    setInputFieldToUnusedTable(entity, rowIndex) {
        if (rowIndex == null) throw "rowIndex may not be null!";
        const inputField = this.getInputField(entity);
        if (inputField == null) { // Entity has no inputField, the space is empty
            return;
        }
        inputField.id = UNUSED_TABLE.createInputFieldId(rowIndex);
        inputField.name = "unusedDemanders[" + rowIndex + "].id";
    },
    setInputFieldToMainTable(entity, colIndex, rowIndex) {
        if (rowIndex == null) throw "rowIndex may not be null!";
        if (colIndex == null) throw "colIndex may not be null!";
        const inputField = this.getInputField(entity);
        if (inputField == null) { // Entity has no inputField, the space is empty
            return;
        }
        inputField.id = MAIN_TABLE.createInputFieldId(colIndex, rowIndex);
        inputField.name = "dataGroupList[" + colIndex + "].dataMemberList[" + rowIndex + "].demander.id";
    },
    getInputField(entity) {
        const inputField = getFirstChildByClassname(entity, INPUT_FIELD_CLASS);
        return inputField;
    }
}

/* Entities die auch wirklich einen demander haben! */
CONTENT = {
}