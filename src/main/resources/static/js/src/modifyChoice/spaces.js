const NO_WAY_VALUE = '---';

let SPACES = {
    getDemanderId(space, nullable = false) {
        const element = space.lastElementChild;
        return ENTITIES.getDemanderId(element, nullable);
    },
    getOfferId(space, nullable) {
        const colIndex = mainTableWrapper.getColIndex(space, nullable);
        if (colIndex == null) return null;
        return mainTableWrapper.getOfferIdByIndex(colIndex);
    },
    /* Einer von beiden benoetigt */
    isNoWaySpace(space = null, valueDiff = SPACES.getValueDiff(space)) {
        return valueDiff === NO_WAY_VALUE;
    },
    getValueDiff(space) {
        return getFirstChildByClassname(getFirstChildByClassname(space, ENTITY_CLASS), OPINION_DIFF_CLASS).innerHTML;
    },
    isEmpty(space) { // Still has an entity but with no content in it
        space.classList.contains(SPACE_EMPTY_CLASS);
    },
    getEntity(space) {
        if (space == null) throw "Space may not be null!"
        const entity = getFirstChildByClassname(space, ENTITY_CLASS);
        if (entity == null) throw "Could not find entity of space = "+space+"!";
        return entity;
    },
    resetValueDiff(space) {
        ENTITIES.resetValueDiff(this.getEntity(space));
    },
    resetAllValueDiffs() {
        for (let offerIndex = 0; offerIndex < mainTable.rows[0].cells.length; offerIndex++) {
            for (let space of mainTableWrapper.getSpaces(offerIndex)) {
                SPACES.resetValueDiff(space);
            }
        }
        for (let space of unusedTableWrapper.getSpaces()) {
            SPACES.resetValueDiff(space);
        }
    },
    setValueDiff(space, d2_id, o2_id, d2_o2_opinionValue) {
        const entity = SPACES.getEntity(space);
        const d1_id = ENTITIES.getDemanderId(entity, true);
        const o1_id = SPACES.getOfferId(space, true);
        let valueDiff = OPINIONS.calcOpinionValueChange(d1_id, o1_id, d2_id, o2_id, d2_o2_opinionValue);
        if (valueDiff === -Infinity) {
            valueDiff = NO_WAY_VALUE;
        }
        ENTITIES.setValueDiff(entity, valueDiff);
    },
    calcValueDiff(space, d2_id, o2_id, d2_o2_opinionValue) {
        const entity = SPACES.getEntity(space);
        const d1_id = ENTITIES.getDemanderId(entity, true);
        const o1_id = SPACES.getOfferId(space, true);
        return OPINIONS.calcOpinionValueChange(d1_id, o1_id, d2_id, o2_id, d2_o2_opinionValue);
    },
    setAllValueDiffs(space) {
        const demanderId = SPACES.getDemanderId(space);
        const offerId = SPACES.getOfferId(space, true);
        const demander_offer_opinionValue = OPINIONS.getValue(demanderId, offerId);

        console.log("mouseOver; demanderId="+demanderId);

        for (let otherSpace of mainTableWrapper.getSpaces()) {
            this.setValueDiff(otherSpace, demanderId, offerId, demander_offer_opinionValue);
        }

        for (let otherSpace of unusedTableWrapper.getSpaces()) {
            this.setValueDiff(otherSpace, demanderId, offerId, demander_offer_opinionValue);
        }
    },

    setAllMaxValueDiffs() {
        console.log("mouseOver; no demander");

        function setMaxValueDiffs(spaces) {
            for (let space of spaces) {
                const maxValueDiff = SPACES.getMaxValueDiff(space);
                if (maxValueDiff != null) {
                    const otherEntity = SPACES.getEntity(space);
                    ENTITIES.setValueDiff(otherEntity, "best alternative: " + maxValueDiff);
                }
            }
        }

        setMaxValueDiffs(mainTableWrapper.getSpaces());
        setMaxValueDiffs(unusedTableWrapper.getSpaces());
    },
    getMaxValueDiff(space) {
        const demanderId = SPACES.getDemanderId(space, true);
        if (demanderId == null) {
            return null;
        }
        const offerId = SPACES.getOfferId(space, true);
        const demander_offer_opinionValue = OPINIONS.getValue(demanderId, offerId);

        function getMaxValueDiff_comparedToSpaces(spaces) {
            let maxValueDiff = -Infinity;
            for (let otherSpace of spaces) {
                const otherOfferId = SPACES.getOfferId(otherSpace, true);
                if (otherOfferId == null || otherOfferId !== offerId) {
                    const valueDiff = SPACES.calcValueDiff(otherSpace, demanderId, offerId, demander_offer_opinionValue);
                    if (valueDiff > maxValueDiff) {
                        maxValueDiff = valueDiff;
                    }
                }
            }
            return maxValueDiff;
        }

        let maxValueDiff1 = getMaxValueDiff_comparedToSpaces(mainTableWrapper.getSpaces());
        let maxValueDiff2 = getMaxValueDiff_comparedToSpaces(unusedTableWrapper.getSpaces());

        return maxValueDiff1 > maxValueDiff2 ? maxValueDiff1 : maxValueDiff2;
    },

    setFull(space, setEvents = true) {
        space.classList.remove(SPACE_EMPTY_CLASS);
        space.classList.remove(SPACE_HOVERED_CLASS);
        space.classList.remove(SPACE_HOVERED_NO_WAY_CLASS);
        space.classList.add(SPACE_FULL_CLASS);
        if (setEvents) {
            this.removeEmptyEvents(space);
            this.addFullEvents(space);
        }
    },
    setEmpty(space) {
        space.classList.remove(SPACE_FULL_CLASS);
        space.classList.add(SPACE_EMPTY_CLASS);
        space.classList.remove(SPACE_HOVERED_CLASS);
        space.classList.remove(SPACE_HOVERED_NO_WAY_CLASS);
        this.removeFullEvents(space);
        this.addEmptyEvents(space);
    },
    updateJava(space) {
        if (SPACES.isEmpty(space)) { // if Space has no entity with an insertField
            return;
        }
        const entity = SPACES.getEntity(space);
        const rowIndex = mainTableWrapper.getRowIndex(space, true);
        const inUnusedTable = rowIndex == null;
        if (inUnusedTable) {
            const unusedTableRowIndex = unusedTableWrapper.getRowIndex(space);
            ENTITIES.setInputFieldToUnusedTable(entity, unusedTableRowIndex);
        } else {
            const colIndex = MAIN_TABLE.getColIndex(mainTable.rows[rowIndex], space);
            ENTITIES.setInputFieldToMainTable(entity, colIndex, rowIndex);
        }
    },

    addFullEvents(space) {
        space.addEventListener('dragleave', dragLeaveFull);
        space.addEventListener('dragover', dragOver);
        space.addEventListener('dragenter', dragEnterFull);
        space.addEventListener('drop', dragDropFull);

        space.addEventListener('mouseover', mouseOverFull);
        space.addEventListener('mouseleave', mouseLeaveFull);
    },
    removeFullEvents(space) {
        space.removeEventListener('dragleave', dragLeaveFull);
        space.removeEventListener('dragover', dragOver);
        space.removeEventListener('dragenter', dragEnterFull);
        space.removeEventListener('drop', dragDropFull);

        space.removeEventListener('mouseover', mouseOverFull);
        space.removeEventListener('mouseleave', mouseLeaveFull);
    },
    addEmptyEvents(space) {
        space.addEventListener('dragleave', dragLeaveEmpty);
        space.addEventListener('dragover', dragOver);
        space.addEventListener('dragenter', dragEnterEmpty);
        space.addEventListener('drop', dragDropEmpty);
    },
    removeEmptyEvents(space) {
        space.removeEventListener('dragleave', dragLeaveEmpty);
        space.removeEventListener('dragover', dragOver);
        space.removeEventListener('dragenter', dragEnterEmpty);
        space.removeEventListener('drop', dragDropEmpty);
    }
}
