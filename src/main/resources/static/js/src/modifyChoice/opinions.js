const OPINIONS = {
    getValue(demanderId, offerId) {
        if (demanderId == null || offerId == null) {
            return 0;
        }
        for (let opinion of opinionArray) {
            if (opinion.demanderId === demanderId && opinion.offerId === offerId) {
                if (opinion.value === -1) {
                    return -Infinity;
                }
                return opinion.value + addDemanderBonus;
            }
        }
        throw "Couldn't find opinion with demanderId="+demanderId+" and offerId="+offerId;
    },
    addToValueSum(addValue) {
        const valueElement = document.getElementById("value");
        const valueSum = valueElement.innerHTML;
        if (Number(valueSum) === -Infinity) {
            OPINIONS.setValueSum(valueElement);
        } else {
            valueElement.innerHTML = (Number(valueElement.innerHTML) + Number(addValue)).toString();
        }
    },
    setValueSum(valueElement = document.getElementById("value")) {
        let valueSum = 0;
        for (let content of contentArr) {
            valueSum += ENTITIES.calcValue(content, false);
        }
        valueElement.innerHTML = valueSum.toString();
    },
    /* Everything except the opinionValue may be null */
    calcOpinionValueChange(d1_id, o1_id, d2_id, o2_id, d2_o2_opinionValue) {
        if (o1_id == null && o2_id == null) { // Both in unusedTable
            return 0;
        } else if (o1_id != null && o2_id != null && o1_id === o2_id) { // Both in same column/offer/group
            return 0;
        } else {
            const d1_o1_opinionValue = OPINIONS.getValue(d1_id, o1_id);
            const d1_o2_opinionValue = OPINIONS.getValue(d1_id, o2_id);
            const d2_o1_opinionValue = OPINIONS.getValue(d2_id, o1_id);
            return d2_o1_opinionValue + d1_o2_opinionValue - d1_o1_opinionValue - d2_o2_opinionValue;
        }
    }/*,
    getOpinionValueByCell: function(cell) {
        if (cell === undefined) throw "Cell may not be undefined.";
        const colIndex = getColIndex2(mainTable.rows, cell);
        if (colIndex === undefined || colIndex === null) throw "Offer colIndex="+colIndex; //todo loeschen
        const offerCell = mainTable.rows[0].cells[colIndex];
        if (offerCell === undefined || offerCell === null) throw "Offer cell="+offerCell; //todo loeschen
        return getOpinionValue(getDemanderIdBySpace(cell), getOfferIdBySpace(offerCell));
    }*/
}

function Opinion(demanderId, offerId, value) {
    this.demanderId = demanderId;
    this.offerId = offerId;
    this.value = value;
}