package an.decider.calc.help;

import an.decider.calc.Choice;
import an.decider.calc.Group;
import an.decider.calc.Member;
import an.decider.calc.input.Demander;
import an.decider.calc.methods.Opinions;

import java.util.ArrayList;
import java.util.List;

public class TestChoice extends Choice {
    private final IdNameGenerator offerGen = new IdNameGenerator("offer", "");
    private final IdNameGenerator demanderGen = new IdNameGenerator("demander", "");
    private final double addDemanderBonus;

    public TestChoice(double addDemanderBonus) {
        super(addDemanderBonus, new ArrayList<>(), new ArrayList<>());
        this.addDemanderBonus = addDemanderBonus;
    }

    @Override
    public boolean optimize() {
        List<Demander> demanders = new ArrayList<>();
        for (Member member : getMembers()) {
            demanders.add(member.getDemander());
        }
        return super.optimize();
    }

    public TestOffer addOffer(boolean hasToBeIncluded, Integer available, int minGroupSize, Integer maxGroupSize) {
        TestOffer offer = new TestOffer(offerGen.newId(), offerGen.newName(), hasToBeIncluded, available, minGroupSize, maxGroupSize);
        for (int i = 0; i < available; i++) {
            Group group = new Group(offer);
            getGroups().add(group);
        }
        return offer;
    }

    public TestDemander addDemander(boolean hasToBeIncluded) {
        TestDemander demander = new TestDemander(demanderGen.newId(), demanderGen.newName(), hasToBeIncluded);
        Member member = new Member(addDemanderBonus, demander);
        getMembers().add(member);
        return demander;
    }
}
