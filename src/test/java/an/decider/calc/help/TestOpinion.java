package an.decider.calc.help;

import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import an.decider.calc.input.Opinion;

public class TestOpinion implements Opinion {
    private final Demander demander;
    private final Offer offer;
    private final double value;

    public TestOpinion(Demander demander, Offer offer, double value) {
        this.demander = demander;
        this.offer = offer;
        this.value = value;
    }

    @Override
    public Offer getOffer() {
        return offer;
    }

    @Override
    public Demander getDemander() {
        return demander;
    }

    @Override
    public Double getValue() {
        return value;
    }
}
