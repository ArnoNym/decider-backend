package an.decider.calc.help;

import an.decider.data.model.input.Offer;

public class TestOffer extends Offer {
    public TestOffer() {
    }

    public TestOffer(String id, String name, boolean hasToBeIncluded, Integer available, int minGroupSize, Integer maxGroupSize) {
        super(id, name, hasToBeIncluded, available, minGroupSize, maxGroupSize);
    }
}
