package an.decider.calc.help;

public class IdNameGenerator {
    private String idAppendFront;
    private String idAppendBack;
    private String nameAppendFront;
    private String nameAppendBack;

    private int count = 0;
    private boolean idUsed = false;
    private boolean nameUsed = false;

    public IdNameGenerator(String idAppendFront, String idAppendBack, String nameAppendFront, String nameAppendBack) {
        this.idAppendFront = idAppendFront;
        this.idAppendBack = idAppendBack;
        this.nameAppendFront = nameAppendFront;
        this.nameAppendBack = nameAppendBack;
    }

    public IdNameGenerator(String appendFront, String appendBack) {
        this.idAppendFront = this.nameAppendFront = appendFront;
        this.idAppendBack = this.nameAppendBack = appendBack;
    }

    private void increment() {
        count++;
        idUsed = false;
        nameUsed = false;
    }

    public String newId() {
        if (idUsed) {
            increment();
        }
        idUsed = true;
        return idAppendFront + "_" + count + "_" + idAppendBack;
    }

    public String newName() {
        if (nameUsed) {
            increment();
        }
        nameUsed = true;
        return nameAppendFront + "_" + count + "_" + nameAppendBack;
    }
}
