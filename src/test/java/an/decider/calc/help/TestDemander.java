package an.decider.calc.help;

import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import an.decider.calc.input.Opinion;
import an.decider.calc.methods.Opinions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TestDemander implements Demander {
    private final IdNameGenerator opinionGen;
    private boolean hasToBeIncluded;
    private final List<Opinion> opinions = new ArrayList<>();

    public TestDemander(String id, String name, boolean hasToBeIncluded) {
        this.hasToBeIncluded = hasToBeIncluded;
        opinionGen = new IdNameGenerator("opinion_of" + id, "");
    }

    @Override
    public Boolean getHasToBeIncluded() {
        return hasToBeIncluded;
    }

    @Override
    public Collection<? extends Opinion> getOpinions() {
        return opinions;
    }

    public Opinion addOpinion(Offer offer, int value) {
        return addOpinion(offer, (double) value);
    }

    public Opinion addOpinion(Offer offer, double value) {
        Opinion opinion = new TestOpinion(this, offer, value);
        opinions.add(opinion);
        return opinion;
    }
}
