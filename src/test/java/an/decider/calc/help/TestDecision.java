package an.decider.calc.help;

import an.decider.calc.Choice;
import an.decider.calc.Decider;
import an.decider.calc.input.Decision;
import an.decider.calc.input.Offer;

import java.util.ArrayList;
import java.util.List;

public class TestDecision implements Decision {
    private final IdNameGenerator offerGen = new IdNameGenerator("offer", "");
    private final IdNameGenerator demanderGen = new IdNameGenerator("demander", "");

    private double addDemanderValue;
    private Integer maxOpinionValue;
    private Integer minGroupCount;
    private Integer maxGroupCount;
    private List<TestOffer> offerList = new ArrayList<>();
    private List<TestDemander> demanderList = new ArrayList<>();

    public TestDecision(Integer maxOpinionValue, Integer minGroupCount, Integer maxGroupCount) {
        this.maxOpinionValue = maxOpinionValue;
        this.minGroupCount = minGroupCount;
        this.maxGroupCount = maxGroupCount;
    }

    public TestOffer addOffer(boolean hasToBeIncluded, Integer available, int minGroupSize, Integer maxGroupSize) {
        TestOffer offer = new TestOffer(offerGen.newId(), offerGen.newName(), hasToBeIncluded, available, minGroupSize, maxGroupSize);
        getOfferList().add(offer);
        return offer;
    }

    public TestDemander addDemander(boolean hasToBeIncluded) {
        TestDemander demander = new TestDemander(demanderGen.newId(), demanderGen.newName(), hasToBeIncluded);
        getDemanderList().add(demander);
        return demander;
    }

    public List<Offer> getOffers() {
        return new ArrayList<>(getOfferList());
    }

    public List<Choice> calculateChoices() {
        return Decider.calculateChoices(this);
    }

    @Override
    public Integer getMaxOpinionValue() {
        return maxGroupCount;
    }

    @Override
    public List<TestOffer> getOfferList() {
        return offerList;
    }

    @Override
    public List<TestDemander> getDemanderList() {
        return demanderList;
    }

    @Override
    public Double getAddDemanderBonus() {
        return addDemanderValue;
    }
}
