package an.decider.calc;

import an.decider.calc.help.TestChoice;
import an.decider.calc.help.TestDemander;
import an.decider.calc.help.TestOffer;
import org.junit.Test;

import java.util.Set;

public class ChoiceTest {
    @Test
    public void choice_01() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 1, 1, null);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 3);
        d1.addOpinion(o2, 3);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, 4);
        d2.addOpinion(o2, 4);

        choice.optimize();

        assert choice.getGroups().size() == 2;
        for (Member member : choice.getMembers()) {
            assert member.isInGroup();
        }
        assert 2 == choice.getGroups().stream().map(Group::getMembers).mapToInt(Set::size).sum();
        assert choice.calcUtility() == 7 + addDemanderBonus*2: choice.calcUtility();
    }

    @Test
    public void choice_02() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 2, 1, 1);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 3);
        d1.addOpinion(o2, 3);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, -1);
        d2.addOpinion(o2, 4);

        choice.optimize();

        assert choice.getGroups().size() == 3;
        for (Member member : choice.getMembers()) {
            assert member.isInGroup();
        }
        assert 2 == choice.getGroups().stream().map(Group::getMembers).mapToInt(Set::size).sum();
        assert choice.calcUtility() == 7 + addDemanderBonus*2: choice.calcUtility();
    }

    @Test
    public void choice_03() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 2, 1, 1);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 3);
        d1.addOpinion(o2, 6);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, -1);
        d2.addOpinion(o2, 4);

        choice.optimize();

        assert choice.getGroups().size() == 3;
        for (Member member : choice.getMembers()) {
            assert member.isInGroup();
        }
        assert 2 == choice.getGroups().stream().map(Group::getMembers).mapToInt(Set::size).sum();
        assert choice.calcUtility() == 10 + addDemanderBonus*2: choice.calcUtility();
    }

    @Test
    public void choice_04() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 2, 1, 1);

        TestDemander d0 = choice.addDemander(false);
        d0.addOpinion(o1, 3);
        d0.addOpinion(o2, 4);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 3);
        d1.addOpinion(o2, 6);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, -1);
        d2.addOpinion(o2, 4);

        choice.optimize();

        assert choice.getGroups().size() == 3;
        for (Member member : choice.getMembers()) {
            assert member.isInGroup() : member.getDemander().equals(d2);
        }
        assert 3 == choice.getGroups().stream().map(Group::getMembers).mapToInt(Set::size).sum();
        assert choice.calcUtility() == 13 + addDemanderBonus*3 : choice.calcUtility();
    }

    @Test
    public void choice_06() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, 1);
        TestOffer o2 = choice.addOffer(false, 2, 1, 1);

        TestDemander d0 = choice.addDemander(false);
        d0.addOpinion(o1, 3);
        d0.addOpinion(o2, 6);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 3);
        d1.addOpinion(o2, 6);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, -1);
        d2.addOpinion(o2, 0);

        choice.optimize();

        assert choice.getGroups().size() == 3;
        for (Member member : choice.getMembers()) {
            assert member.isInGroup() : member.getDemander().equals(d2);
        }
        assert 3 == choice.getGroups().stream().map(Group::getMembers).mapToInt(Set::size).sum();
        assert choice.calcUtility() == 3+6+0 + addDemanderBonus*3 : choice.calcUtility();
    }


    @Test
    public void choice_0() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 2, 1, 1);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 4);
        d1.addOpinion(o2, 7);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, 2);
        d2.addOpinion(o2, 8);

        TestDemander d3 = choice.addDemander(false);
        d3.addOpinion(o1, 9);
        d3.addOpinion(o2, 1);

        TestDemander d4 = choice.addDemander(false);
        d4.addOpinion(o1, 2);
        d4.addOpinion(o2, 3);

        choice.optimize();

        assert choice.getGroups().size() == 3;
        for (Member member : choice.getMembers()) {
            assert member.isInGroup();
        }
        assert 4 == choice.getGroups().stream().map(Group::getMembers).mapToInt(Set::size).sum();
        assert choice.calcUtility() == 7+8+9+2 + addDemanderBonus*4: choice.calcUtility();
    }

    @Test
    public void choice_1() {
        TestChoice choice = new TestChoice(5);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 2, 1, 1);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 7);
        d1.addOpinion(o2, 7);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, 7);
        d2.addOpinion(o2, 7);

        TestDemander d3 = choice.addDemander(false);
        d3.addOpinion(o1, 7);
        d3.addOpinion(o2, 7);

        TestDemander d4 = choice.addDemander(false);
        d4.addOpinion(o1, 7);
        d4.addOpinion(o2, 7);

        choice.optimize();

        assert choice.getGroups().size() == 3;
        for (Member member : choice.getMembers()) {
            assert member.isInGroup();
        }
        assert 4 == choice.getGroups().stream().map(Group::getMembers).mapToInt(Set::size).sum();
        assert choice.calcUtility() == 4*7 + 4*5;
    }

    @Test
    public void choice_2() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 1, 1, null);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, -1);
        d1.addOpinion(o2, 10);

        TestDemander d2 = choice.addDemander(false);
        d2.addOpinion(o1, 5);
        d2.addOpinion(o2, -1);

        choice.optimize();

        assert choice.calcUtility() == 15 + addDemanderBonus * 2;

        //assert choice.getGroups().
    }

    @Test
    public void choice_3() {
        double addDemanderBonus = 5;

        TestChoice choice = new TestChoice(addDemanderBonus);
        TestOffer o1 = choice.addOffer(false, 1, 1, null);
        TestOffer o2 = choice.addOffer(false, 1, 1, null);

        TestDemander d1 = choice.addDemander(false);
        d1.addOpinion(o1, 7);
        d1.addOpinion(o2, 7);

        TestDemander d2 = choice.addDemander(true);
        d2.addOpinion(o1, 4);
        d2.addOpinion(o2, 4);

        TestDemander d3 = choice.addDemander(false);
        d3.addOpinion(o1, 4);
        d3.addOpinion(o2, 4);

        TestDemander d4 = choice.addDemander(false);
        d4.addOpinion(o1, 7);
        d4.addOpinion(o2, 7);

        choice.optimize();

        assert choice.calcUtility() == 22 + 4*addDemanderBonus : choice.calcUtility();

        //assert choice.getGroups().
    }


    @Test
    public void runden() {
        System.out.println(Math.round(1.3));
        System.out.println(Math.round(1.7));
        System.out.println(Math.ceil(1.3));
        System.out.println(Math.ceil(1.7));
    }
}
