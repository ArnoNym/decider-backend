package an.decider.calc;

import an.decider.utils.SetUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class SetUtilsTest {


    @Test
    public void joinMaxUtilityGroup() {
        StringBuilder sb = new StringBuilder();
        List<Integer> objects = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            objects.add(i);
        }
        Set<List<Integer>> miniSets = SetUtils.createPowerSet(objects, 1, null);
        for (Collection<Integer> miniSet : miniSets) {
            sb.append("// ");
            for (Integer i : miniSet) {
                sb.append(i).append(", ");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
}
