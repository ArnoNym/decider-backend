package an.decider.calc;

import an.decider.calc.help.TestChoice;
import an.decider.calc.help.TestDecision;
import an.decider.calc.help.TestDemander;
import an.decider.calc.help.TestOffer;
import an.decider.calc.input.Demander;
import an.decider.calc.input.Offer;
import org.h2.jdbc.JdbcSQLSyntaxErrorException;
import org.junit.*;

import java.util.List;
import java.util.Random;

public class DeciderTest {
    @BeforeClass
    public static void setup() {
    }

    private static TestDecision decision;
    @Before
    public void setupThis() {
        decision = new TestDecision(10, 1, null);
    }

    @After
    public void tearThis() {
    }

    @AfterClass
    public static void tear() {
    }

    @Test
    public void createSortedChoices_0() {
        assert decision.calculateChoices().isEmpty();
    }

    @Test
    public void createSortedChoices_1() {
        TestOffer o1 = decision.addOffer(false, 1, 1, null);

        TestDemander d1 = decision.addDemander(false);
        d1.addOpinion(o1, 5);

        List<Choice> choices = decision.calculateChoices();
        for (Choice choice : choices) {
            System.out.println(choice.toString());
        }
        assert choices.size() == 1 : choices.size();
        assert choices.get(0).getGroups().size() == 1;
        assert choices.get(0).getMembers().size() == 1;
    }

    @Test
    public void createSortedChoices_2() {
        TestOffer o1 = decision.addOffer(false, 1, 1, null);
        TestOffer o2 = decision.addOffer(false, 1, 1, null);

        TestDemander d1 = decision.addDemander(false);
        d1.addOpinion(o1, 5);
        d1.addOpinion(o2, 10);

        List<Choice> choices = decision.calculateChoices();
        //assert choices.get(0).getGroups().size() == 1;
        //assert choices.get(0).getMembers().size() == 1;

        for (Choice choice : choices) {
            System.out.println(choice.toString());
        }
    }

    @Test
    public void createSortedChoices_3() {
        TestOffer o1 = decision.addOffer(false, 1, 1, 1);
        TestOffer o2 = decision.addOffer(false, null, 1, 1);

        TestDemander d1 = decision.addDemander(false);
        d1.addOpinion(o1, 2);
        d1.addOpinion(o2, 5);

        TestDemander d2 = decision.addDemander(false);
        d2.addOpinion(o1, 2);
        d2.addOpinion(o2, 5);

        TestDemander d3 = decision.addDemander(false);
        d3.addOpinion(o1, 4);
        d3.addOpinion(o2, 5);

        List<Choice> choices = decision.calculateChoices();
        for (Choice choice : choices) {
            System.out.println(choice.toString());
        }
        assert choices.size() == 3 : choices.size();
        assert choices.stream().mapToInt(choice -> choice.getGroups().size()).sum() == 1 + 1 + 4;
    }

    @Test
    public void scaling_1() {
        int demanderCount = 2;
        int offerCount = 2;

        Random random = new Random();
        for (int i = 0; i < offerCount; i++) {
            int minGroupSize = random.nextInt(demanderCount) + 1;
            Integer maxGroupSize = null;
            decision.addOffer(false, random.nextInt(demanderCount) + 1, minGroupSize, maxGroupSize);
        }
        for (int i = 0; i < demanderCount; i++) {
            TestDemander demander = decision.addDemander(false);
            for (Offer offer : decision.getOfferList()) {
                demander.addOpinion(offer, (double) (random.nextInt(110) / 10 - 1));
            }
        }
        List<Choice> choices = decision.calculateChoices();
    }


    /*
    @Test
    public void createSortedChoices_2() {
        Decision decision = new Decision("decision-name", 1, null);

        // Offers =====================================================================================================

        Offer o1 = new Offer(1, "o1", false, 1, 1, null);
        decision.getOfferList().add(o1);

        Offer o2 = new Offer(2,"o2", false, 1, 1, null);
        decision.getOfferList().add(o2);

        // Demanders =================================================================================

        IdGenerator oig = new IdGenerator();

        Demander d1 = new Demander(1, "d1", false);
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o1, 10));
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o2, 8));
        decision.getDemanderList().add(d1);

        decision.validate();

        Demander d2 = new Demander(2, "d2", false);
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o1, 10));
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o2, 5));
        decision.getDemanderList().add(d2);

        decision.validate();

        Demander d3 = new Demander(3, "d3", false);
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o1, 10));
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o2, 5));
        decision.getDemanderList().add(d3);

        decision.validate();

        Demander d4 = new Demander(4, "d4", false);
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o1, 7));
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o2, 2));
        decision.getDemanderList().add(d4);

        decision.validate();

        Decider.createChoiceList(decision);
    }

    @Test
    public void createSortedChoices_3() {
        Decision decision = new Decision("decision-name", 1, null);

        // Offers =====================================================================================================

        Offer o1 = new Offer(1, "o1", false, 1, 1, null);
        decision.getOfferList().add(o1);

        Offer o2 = new Offer(2,"o2", false, 1, 1, null);
        decision.getOfferList().add(o2);

        Offer o3 = new Offer(3, "o3", false, 1, 1, null);
        decision.getOfferList().add(o3);

        // Demanders =================================================================================

        IdGenerator oig = new IdGenerator();

        Demander d1 = new Demander(1, "d1", false);
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o1, 1));
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o2, 10));
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o3, 1));
        decision.getDemanderList().add(d1);

        decision.validate();

        Demander d2 = new Demander(2, "d2", false);
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o1, 1));
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o2, 6));
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o3, 1));
        decision.getDemanderList().add(d2);

        decision.validate();

        Demander d3 = new Demander(3, "d3", false);
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o1, 1));
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o2, 4));
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o3, 1));
        decision.getDemanderList().add(d3);

        decision.validate();

        Demander d4 = new Demander(4, "d4", false);
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o1, 1));
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o2, 3));
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o3, 1));
        decision.getDemanderList().add(d4);

        decision.validate();

        Decider.createChoiceList(decision);
    }

    @Test
    public void muster() {
        Decision decision = new Decision("decision-name", 1, null);

        // Offers =====================================================================================================

        Offer o1 = new Offer(1, "o1", false, 1, 1, null);
        decision.getOfferList().add(o1);

        Offer o2 = new Offer(2,"o2", false, 1, 1, null);
        decision.getOfferList().add(o2);

        Offer o3 = new Offer(3, "o3", false, 1, 1, null);
        decision.getOfferList().add(o3);

        Offer o4 = new Offer(4, "o4", false, 1, 1, null);
        decision.getOfferList().add(o4);

        // Demanders =================================================================================

        IdGenerator oig = new IdGenerator();

        Demander d1 = new Demander(1, "d1", false);
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o1, 5));
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o2, 5));
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o3, 5));
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o4, 5));
        decision.getDemanderList().add(d1);

        decision.validate();

        Demander d2 = new Demander(2, "d2", false);
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o1, 5));
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o2, 5));
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o3, 5));
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o4, 5));
        decision.getDemanderList().add(d2);

        decision.validate();

        Demander d3 = new Demander(3, "d3", false);
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o1, 5));
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o2, 5));
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o3, 5));
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o4, 5));
        decision.getDemanderList().add(d3);

        decision.validate();

        Demander d4 = new Demander(4, "d4", false);
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o1, 5));
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o2, 5));
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o3, 5));
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o4, 5));
        decision.getDemanderList().add(d4);

        decision.validate();

        Demander d5 = new Demander(5, "d5", false);
        d5.getOpinionList().add(new Opinion(oig.next(), d5, o1, 5));
        d5.getOpinionList().add(new Opinion(oig.next(), d5, o2, 5));
        d5.getOpinionList().add(new Opinion(oig.next(), d5, o3, 5));
        d5.getOpinionList().add(new Opinion(oig.next(), d5, o4, 5));
        decision.getDemanderList().add(d5);

        decision.validate();

        Demander d6 = new Demander(6, "d6", false);
        d6.getOpinionList().add(new Opinion(oig.next(), d6, o1, 5));
        d6.getOpinionList().add(new Opinion(oig.next(), d6, o2, 5));
        d6.getOpinionList().add(new Opinion(oig.next(), d6, o3, 5));
        d6.getOpinionList().add(new Opinion(oig.next(), d6, o4, 5));
        decision.getDemanderList().add(d6);

        decision.validate();

        Demander d7 = new Demander(7, "d7", false);
        d7.getOpinionList().add(new Opinion(oig.next(), d7, o1, 5));
        d7.getOpinionList().add(new Opinion(oig.next(), d7, o2, 5));
        d7.getOpinionList().add(new Opinion(oig.next(), d7, o3, 5));
        d7.getOpinionList().add(new Opinion(oig.next(), d7, o4, 5));
        decision.getDemanderList().add(d7);

        decision.validate();

        Decider.createChoiceList(decision);
    }

    /*
    @Test
    public void bestellungen() {
        Manager manager = new Manager();
        manager.setMinGroupCount(2);
        manager.setMinGroupCount(null);

        // Offers =================================================================================

        Offer italPizza = new Offer("italPizza", 1, 3, null);
        manager.addOffer(italPizza);

        Offer amerikaPizza = new Offer("amerikaPizza", 1, 3, null);
        manager.addOffer(amerikaPizza);

        Offer fanzy = new Offer("fanzy", 1, 3, null);
        manager.addOffer(fanzy);

        Offer anahong = new Offer("anahong", 1, 3, null);
        manager.addOffer(anahong);

        // Demanders =================================================================================

        Demander l = new Demander("L");
        l.setInsistsOnGroup(true);
        l.putOpinion(italPizza, 5);
        l.putOpinion(amerikaPizza, 8);
        l.putOpinion(fanzy, 4);
        l.putOpinion(anahong, 10);
        manager.addDemander(l);

        Demander tim = new Demander("Tim");
        l.setInsistsOnGroup(true);
        tim.putOpinion(italPizza, 7);
        tim.putOpinion(amerikaPizza, 4);
        tim.putOpinion(fanzy, 3);
        tim.putOpinion(anahong, 4);
        manager.addDemander(tim);

        Demander koenig = new Demander("Koenig");
        l.setInsistsOnGroup(true);
        koenig.putOpinion(italPizza, 7);
        koenig.putOpinion(amerikaPizza, 8);
        koenig.putOpinion(fanzy, 8);
        koenig.putOpinion(anahong, 7);
        manager.addDemander(koenig);

        Demander hendrik = new Demander("Hendrik");
        l.setInsistsOnGroup(true);
        hendrik.putOpinion(italPizza, 8);
        hendrik.putOpinion(amerikaPizza, -1);
        hendrik.putOpinion(fanzy, -1);
        hendrik.putOpinion(anahong, -1);
        manager.addDemander(hendrik);

        Demander zora = new Demander("Zora");
        l.setInsistsOnGroup(true);
        zora.putOpinion(italPizza, 6);
        zora.putOpinion(amerikaPizza, 6);
        zora.putOpinion(fanzy, 4);
        zora.putOpinion(anahong, 4);
        manager.addDemander(zora);

        Demander annCathrin = new Demander("Ann-Cathrin");
        l.setInsistsOnGroup(true);
        annCathrin.putOpinion(italPizza, 8);
        annCathrin.putOpinion(amerikaPizza, 5);
        annCathrin.putOpinion(fanzy, 5);
        annCathrin.putOpinion(anahong, 5);
        manager.addDemander(annCathrin);

        Demander rico = new Demander("Rico");
        rico.putOpinion(italPizza, 10);
        rico.putOpinion(amerikaPizza, 1);
        rico.putOpinion(fanzy, 1);
        rico.putOpinion(anahong, 1);
        manager.addDemander(rico);

        List<Choice> choices = manager.createChoices();
        for (Choice choice : choices) {
            System.out.println(choice.toString() + "\n");
        }
    }
    *

    @Test
    void createSortedChoices_10() {
        Decision decision = new Decision("decision-name", 1, null);

        // Offers =====================================================================================================

        Offer o1 = new Offer(1, "o1", false, 1, 1, null);
        decision.getOfferList().add(o1);

        Offer o2 = new Offer(2,"o2", false, 1, 1, null);
        decision.getOfferList().add(o2);

        // Demanders =================================================================================

        IdGenerator oig = new IdGenerator();

        Demander d1 = new Demander(1, "d1", false);
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o1, -1));
        d1.getOpinionList().add(new Opinion(oig.next(), d1, o2, 1));
        decision.getDemanderList().add(d1);

        decision.validate();

        Demander d2 = new Demander(2, "d2", false);
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o1, -1));
        d2.getOpinionList().add(new Opinion(oig.next(), d2, o2, 1));
        decision.getDemanderList().add(d2);

        decision.validate();

        Demander d3 = new Demander(3, "d3", false);
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o1, 1));
        d3.getOpinionList().add(new Opinion(oig.next(), d3, o2, 1));
        decision.getDemanderList().add(d3);

        decision.validate();

        Demander d4 = new Demander(4, "d4", false);
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o1, 1));
        d4.getOpinionList().add(new Opinion(oig.next(), d4, o2, 1));
        decision.getDemanderList().add(d4);

        decision.validate();

        Decider.createChoiceList(decision);
    }
    */
}